# Shared Blogs App
A blogging platform for creating, sharing, and exploring diverse perspectives and insights, fostering knowledge exchange and community engagement.

## Link: <a href="https://ishare-85651.firebaseapp.com/"> Shared Blogs</a>




## Features

- **Authentication**: Users can sign up and sign in securely.
- **Profile Management**: Users can view all profiles and edit their own.
- **Post Management**: Users can create, edit, and view blog-posts.
- **Reporting System**: Users can report inappropriate content.
- **Requesting System**: Users can send optional requests to Admins.
- **Blocking System**: Admins can block other users and blog-posts if necessary.
- **Dashboard**: Admins have access to a personalized dashboard.
- **Private Chat with Admin**: Users can initiate private conversations with admins.
- **User Management**: Admins can manage users and their permissions.
- **Messaging System**: Users can communicate with each other through messages. (Coming soon!)

## Technologies Used

- **React**
- **React Router**
- **Redux Toolkit**
- **Firebase**
- **Chakra UI**
- **Framer Motion**
- **React Quill**
- **React Responsive Carousel**
- **ES6+ JavaScript**

## Getting Started
To run this application locally, follow these steps:

1. Clone this repository to your local machine.

 ```bash
 git clone https://gitlab.com/Silvia_Gelusheva/shared-blogs-app.git

   ```

2. Navigate to the project directory.

```bash
cd shared-blogs
```


3. Install dependencies using npm or yarn..

```bash
npm install
# or
yarn install

```

# Test

User:  
e-mail: *user@test.com*  
password: 123456

Admin:  
e-mail: *admin@test.com*  
password: 123456  

... or sign up as a new user if you prefer.


<br>

#### **Pages**

<div style="display: flex; justify-content: start; gap: 2rem">
    <img src="./images/Home_2.png"width="400px">
    <img src="./images/Home.png"width="400px">
</div>
<br/>
<div style="display: flex; justify-content: start; gap: 2rem"> 
    <img src="./images/Post.png"width="400px">
    <img src="./images/Saved.png"width="400px"> 
</div>
<br/>
<div style="display: flex; justify-content: start; gap: 2rem">   
    <img src="./images/Users.png"width="400px">  
    <img src="./images/Profile.png"width="400px"> 
</div>
<br/>
<div style="display: flex; justify-content: start; gap: 2rem">
    <img src="./images/Dashboard.png"width="400px">
    <img src="./images/Reports.png"width="400px">   
</div>
<br/>
<div style="display: flex; justify-content: start; gap: 2rem">  
  <img src="./images/Requests.png"width="400px">
  <img src="./images/Blocked.png"width="400px">  
</div>
<br/>
<div style="display: flex; justify-content: start; gap: 2rem">  
  <img src="./images/chatAdmin.png"width="400px">
  <img src="./images/chatUser.png"width="400px">  
</div>


