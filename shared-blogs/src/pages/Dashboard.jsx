import {
  Box,
  CSSReset,
  Stack,
  Tab,
  TabList,
  TabPanel,
  TabPanels,
  Tabs,
  useMediaQuery,
} from "@chakra-ui/react";

import Blocked from "./Blocked";
import Reports from "./Reports";
import Requests from "./Requests";
import Statistics from "./Statistics";

const Dashboard = () => {
  const [isLargerThan768] = useMediaQuery("(min-width: 768px)");

  return (
    <Stack>
      <CSSReset />
      <Box p="4">
        {isLargerThan768 ? (
          <Tabs>
            <TabList fontWeight="semibold">
              <Tab>Statistics</Tab>
              <Tab>Reported</Tab>
              <Tab>Requests</Tab>
              <Tab>Blocked</Tab>
            </TabList>

            <TabPanels>
              <TabPanel>
                <Statistics />
              </TabPanel>
              <TabPanel>
                <Reports />
              </TabPanel>
              <TabPanel>
                <Requests />
              </TabPanel>
              <TabPanel>
                <Blocked />
              </TabPanel>
            </TabPanels>
          </Tabs>
        ) : (
          <Tabs isLazy>
            <TabList fontWeight="semibold" flexWrap="wrap">
              <Tab>Statistics</Tab>
              <Tab>Reported</Tab>
              <Tab>Requests</Tab>
              <Tab>Blocked</Tab>
            </TabList>

            <TabPanels>
              <TabPanel>
                <Statistics />
              </TabPanel>
              <TabPanel>
                <Reports />
              </TabPanel>
              <TabPanel>
                <Requests />
              </TabPanel>
              <TabPanel>
                <Blocked />
              </TabPanel>
            </TabPanels>
          </Tabs>
        )}
      </Box>
    </Stack>
  );
};

export default Dashboard;
