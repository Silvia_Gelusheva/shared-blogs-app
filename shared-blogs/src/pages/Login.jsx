import {
  Box,
  Button,
  Flex,
  FormControl,
  Heading,
  Input,
  Text,
} from "@chakra-ui/react";
import { Link, useNavigate } from "react-router-dom";
import { useEffect, useState } from "react";

import { auth } from "../firebase/firebase.config";
import { logInUser } from "../redux/actions/user-actions";
import { motion } from "framer-motion";
import { onAuthStateChanged } from "firebase/auth";
import { useDispatch } from "react-redux";

const LogIn = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const navigate = useNavigate();
  const dispatch = useDispatch();

  const handleLogin = () => {
    if (!email || !password) {
      alert("Email and password are required.");
      return;
    }
    const emailPattern = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    if (!emailPattern.test(email)) {
      alert("Please enter a valid email address.");
      return;
    }

    dispatch(logInUser({ email, password }));
  };

  useEffect(() => {
    onAuthStateChanged(auth, (currentUser) => {
      if (currentUser) navigate("/");
    });
  }, [dispatch, navigate]);

  const handleKeypress = (e) => {
    if (e.keyCode === 13) {
      handleLogin(e);
    }
  };

  const handleAdminLogin = () => {
    setEmail("admin@test.com");
    setPassword("123456");
    dispatch(logInUser({ email: "admin@test.com", password: "123456" }));
  };

  const handleUserLogin = () => {
    setEmail("user@test.com");
    setPassword("123456");
    dispatch(logInUser({ email: "user@test.com", password: "123456" }));
  };

  return (
    <Flex width="full" height="full" justify="center">
      <motion.div
        initial={{ opacity: 0 }}
        animate={{ opacity: 1 }}
        transition={{ duration: 1 }}
      >
        <Box
          w="lg"
          h="max-content"
          borderWidth="1px"
          borderRadius="lg"
          overflow="hidden"
          p={4}
          m={20}
        >
          <FormControl isRequired>
            <Flex direction="column" gap={4} alignItems="center" w="auto">
              <Heading>Sign In</Heading>
              <Text size="sm">Do not have an account?</Text>
              <Link to="/register">
                <Text color="teal" fontWeight="bold">
                  Sign Up
                </Text>
              </Link>
              <Input
                id="email"
                name="email"
                type="email"
                placeholder="Email"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              />
              <Input
                id="password"
                name="password"
                type="password"
                placeholder="Password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
                onKeyDown={handleKeypress}
              />
            </Flex>
            <Flex justifyContent="end">
              <Button
                mt={4}
                bg="teal"
                type="submit"
                w="full"
                onClick={handleLogin}
              >
                Submit
              </Button>
            </Flex>
          </FormControl>
          <Flex direction="column" gap={2} mt={4}>
            <Button onClick={handleAdminLogin} colorScheme="teal">
              Auto Login as Admin
            </Button>
            <Button onClick={handleUserLogin} colorScheme="teal">
              Auto Login as User
            </Button>
          </Flex>
        </Box>
      </motion.div>
    </Flex>
  );
};

export default LogIn;
