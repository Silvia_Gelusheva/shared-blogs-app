import {
  Avatar,
  Box,
  Button,
  Container,
  Flex,
  FormControl,
  IconButton,
  Input,
  Textarea,
} from "@chakra-ui/react";
import {
  fetchAllUsers,
  updateUserAsyncThunk,
} from "../redux/actions/user-actions";
import {
  getDownloadURL,
  ref as storageRef,
  uploadBytes,
} from "firebase/storage";
import { useDispatch, useSelector } from "react-redux";
import { useEffect, useState } from "react";

import { FaCamera } from "react-icons/fa";
import { storage } from "../firebase/firebase.config";
import { useNavigate } from "react-router-dom";

const EditUser = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const currentUser = useSelector((state) => state.user.user);

  const [newUserData, setNewUserData] = useState({
    avatar: currentUser?.avatar || "",
    name: currentUser?.name || "",
    lastName: currentUser?.lastName || "",
    livesIn: currentUser?.livesIn || "",
    occupation: currentUser?.occupation || "",
    interestedIn: currentUser?.interestedIn || "",
    bio: currentUser?.bio || "",
  });

  // useEffect(() => {
  //   localStorage.setItem("newUserData", JSON.stringify(newUserData));
  // }, [newUserData]);

  const handleAvatarChange = async (e) => {
    const newAvatar = e.target.files[0];
    if (newAvatar) {
      const refToStorage = storageRef(
        storage,
        `avatars/${currentUser.displayName}/avatar`
      );

      const result = await uploadBytes(refToStorage, newAvatar);
      const newAvatarUrl = await getDownloadURL(result.ref);
      setNewUserData((prevUserData) => ({
        ...prevUserData,
        avatar: newAvatarUrl,
      }));
    }
  };

  // Handle avatar click
  const handleAvatarClick = () => {
    document.getElementById("avatarInput").click();
  };

  // Handle update
  const handleUpdate = async () => {
    dispatch(updateUserAsyncThunk(newUserData));
    dispatch(fetchAllUsers());
    navigate(`/users/${currentUser?.displayName}`);
  };

  const inputFields = [
    { name: "name" },
    { name: "lastName" },
    {
      name: "livesIn",
      placeholder: "Currently lives in...",
    },
    {
      name: "occupation",
      placeholder: "Currently works as...",
    },
    {
      name: "interestedIn",
      placeholder: "Interested in...",
    },
  ];

  return (
    <Container
      maxW="container.md"
      borderWidth="2px"
      borderRadius="lg"
      boxShadow="md"
      p={4}
    >
      {/* Avatar section */}
      <Flex direction="column" align="center" justify="center">
        <Input
          id="avatarInput"
          type="file"
          border="none"
          borderColor="none"
          onChange={handleAvatarChange}
          display="none"
        />
        <Box position="relative">
          <Avatar src={newUserData.avatar} size="2xl" cursor="pointer" />
          <IconButton
            rounded="full"
            position="absolute"
            size="sm"
            bottom="0"
            right="0"
            icon={<FaCamera />}
            onClick={handleAvatarClick}
          />
        </Box>
      </Flex>

      {/* Input fields */}
      {inputFields.map((field, index) => (
        <FormControl key={index} my={4}>
          <Input
            type="text"
            color="gray"
            name={field.name}
            cursor="pointer"
            fontStyle="italic"
            fontSize="sm"
            placeholder={field.placeholder || ""}
            value={newUserData[field.name]}
            onChange={(e) =>
              setNewUserData({ ...newUserData, [field.name]: e.target.value })
            }
          />
        </FormControl>
      ))}

      {/* Bio textarea */}
      <Textarea
        placeholder={newUserData.bio || "Tell us about yourself.."}
        color="gray"
        name="bio"
        cursor="pointer"
        fontStyle="italic"
        fontSize="sm"
        value={newUserData.bio}
        onChange={(e) =>
          setNewUserData({ ...newUserData, bio: e.target.value })
        }
      />

      {/* Submit and Back buttons */}
      <Flex w="full" justify="end" mt={6}>
        <Button
          colorScheme="teal"
          size="sm"
          type="submit"
          mr={2}
          onClick={handleUpdate}
        >
          Submit
        </Button>
        <Button colorScheme="teal" size="sm" onClick={() => navigate(-1)}>
          Back
        </Button>
      </Flex>
    </Container>
  );
};

export default EditUser;
