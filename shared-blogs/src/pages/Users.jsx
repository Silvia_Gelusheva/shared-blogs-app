import {
  Divider,
  Flex,
  Heading,
  SimpleGrid,
  Text,
  VStack,
} from "@chakra-ui/react";
import { useDispatch, useSelector } from "react-redux";
import { useEffect, useRef, useState } from "react";

import AdminCard from "../components/cards/AdminCard";
import Pagination from "../components/Pagination";
import SearchBar from "../components/SearchBar";
import UserCard from "../components/cards/UserCard";
import { fetchAllUsers } from "../redux/actions/user-actions";
import { filteredUsers } from "../redux/features/usersSlice";
import { motion } from "framer-motion";
import { selectAllUsers } from "../redux/features/usersSlice";

const Users = () => {
  const dispatch = useDispatch();
  const inputRef = useRef("");
  const users = useSelector((state) => selectAllUsers(state));

  const filterUsers = () => {
    dispatch(filteredUsers(inputRef.current.value));
  };
  useEffect(() => {
    dispatch(fetchAllUsers());
  }, [dispatch]);

  const [currentPage, setCurrentPage] = useState(1);
  const usersPerPage = 8;
  const filteredAndReversedUsers = users
    ? [...users].filter((u) => !u.isBlocked).reverse()
    : [];
  const indexOfLastPost = currentPage * usersPerPage;
  const indexOfFirstPost = indexOfLastPost - usersPerPage;
  const currentUsers = filteredAndReversedUsers?.slice(
    indexOfFirstPost,
    indexOfLastPost
  );

  const totalPages = Math.ceil(filteredAndReversedUsers?.length / usersPerPage);

  const onPageChange = (page) => {
    setCurrentPage(page);
  };

  return (
    <>
        <motion.div
        initial={{ opacity: 0 }}
        animate={{ opacity: 1 }}
        transition={{ duration: 1 }}
      >
      <Flex direction="column" align="center" p={4}>
        <VStack spacing={4} w="full">
          <SearchBar handleChange={filterUsers} inputRef={inputRef} />
          <Heading as="h1" size="2xl">
            Meet The Authors
          </Heading>
          <Text fontSize="lg" color="gray.600">
            Get details about all community members
          </Text>
        </VStack>
        <Divider my={8} />

        <Flex w="full">
          <Pagination
            currentPage={currentPage}
            totalPages={totalPages}
            onPageChange={onPageChange}
          />
        </Flex>

        <SimpleGrid
          columns={{ base: 1, sm: 1, md: 2, lg: 3, xl: 4 }}
          spacing={4}
          w="full"
        >
          {currentUsers.map((user) => (
            <UserCard key={user?.id} user={user} />
          ))}
        </SimpleGrid>
        <Divider my={8} />
        <Heading as="h3" size="xl" mb={4}>
          Admins
        </Heading>
      </Flex>
      <SimpleGrid
        columns={{ base: 1, md: 2, lg: 3, xl: 4 }}
        spacing={4}
        w="full"
      >
        {users
          .filter((u) => u.role === 2)
          .map((user) => (
            <AdminCard key={user?.id} user={user} />
          ))}
      </SimpleGrid>
      </motion.div>
    </>
  );
};

export default Users;
