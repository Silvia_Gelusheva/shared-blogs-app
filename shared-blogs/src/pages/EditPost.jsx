import "react-quill/dist/quill.snow.css";

import {
  Box,
  Button,
  Container,
  Flex,
  GridItem,
  Input,
  SimpleGrid,
  Stack,
  Tooltip,
} from "@chakra-ui/react";
import { getDownloadURL, uploadBytes } from "firebase/storage";
import { useDispatch, useSelector } from "react-redux";

import { AddIcon } from "@chakra-ui/icons";
import ReactQuill from "react-quill";
import { TiDeleteOutline } from "react-icons/ti";
import { storage } from "../firebase/firebase.config";
import { ref as storageRef } from "firebase/storage";
import { updatePostAsyncThunk } from "../redux/actions/post-actions";
import { useNavigate } from "react-router-dom";
import { useState } from "react";
import { v4 } from "uuid";

const EditPost = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const currentPost = useSelector((state) => state.post.post);
  const [images, setImages] = useState([]);
  const [imagePreviews, setImagePreviews] = useState([]);
  const [toggle, setToggle] = useState(false);

  const [newPostData, setNewPostData] = useState({
    urls: currentPost?.urls || [],
    title: currentPost?.title,
    text: currentPost?.text,
  });

  const handleQuillChange = (value) => {
    setNewPostData({ ...newPostData, text: value });
  };

  const handleDeleteExistingImage = (indexToDelete) => {
    setNewPostData({
      ...newPostData,
      urls: newPostData.urls.filter((_, index) => index !== indexToDelete),
    });
  };

  const handleImageChange = (e) => {
    const selectedImages = Array.from(e.target.files);
    setImages((prevImages) => [...prevImages, ...selectedImages]);
    const previews = selectedImages.map((image) => URL.createObjectURL(image));
    setImagePreviews((prevPreviews) => [...prevPreviews, ...previews]);
    setToggle(!toggle);
  };

  const HandleDeleteNewImage = (indexToDelete) => {
    setImages((prevImages) =>
      prevImages.filter((_, index) => index !== indexToDelete)
    );
    setImagePreviews((prevPreviews) =>
      prevPreviews.filter((_, index) => index !== indexToDelete)
    );
  };

  const handleUpdate = async (e) => {
    e.preventDefault();

    try {
      let newPostDataWithImages = { ...newPostData };

      if (images.length > 0) {
        const promises = images.map(async (image) => {
          const storeRef = storageRef(
            storage,
            `posts/${currentPost.author}/${v4()}`
          );
          const result = await uploadBytes(storeRef, image);
          return await getDownloadURL(result.ref);
        });

        const newUrls = await Promise.all(promises);

        newPostDataWithImages = {
          ...newPostDataWithImages,
          urls: [...newPostDataWithImages.urls, ...newUrls],
        };

        setImages([]);
      }

      dispatch(updatePostAsyncThunk([newPostDataWithImages, currentPost]));
      navigate(`/${currentPost?.postId}`);
    } catch (error) {
      console.error("Error updating post:", error.message);
    }
  };

  return (
    <Container maxW="5xl" height="full">
      <Stack
        textAlign="center"
        align="center"
        spacing={{ base: 2, md: 2 }}
        py={{ base: 5, md: 4 }}
        fontSize={25}
      >
        <SimpleGrid spacing={10} columns={{ xl: 5, lg: 4, md: 3, sm: 2 }}>
          {newPostData?.urls.map((image, index) => (
            <GridItem
              key={index}
              backgroundImage={image}
              alt={`Preview ${index}`}
              bgSize="cover"
              bgPosition="center"
              bgRepeat="no-repeat"
              width="200px"
              height="200px"
            >
              <Flex justify="end">
                <Tooltip hasArrow label="Delete image">
                  <Box>
                    <TiDeleteOutline
                      color="white"
                      size={38}
                      onClick={() => handleDeleteExistingImage(index)}
                    />
                  </Box>
                </Tooltip>
              </Flex>
            </GridItem>
          ))}
          {imagePreviews.map((preview, index) => (
            <GridItem
              key={index}
              backgroundImage={preview}
              alt={`Preview ${index}`}
              bgSize="cover"
              bgPosition="center"
              bgRepeat="no-repeat"
              width="200px"
              height="200px"
            >
              <Flex justify="end">
                <Tooltip hasArrow label="Delete image">
                  <Box>
                    <TiDeleteOutline
                      color="white"
                      size={38}
                      onClick={() => HandleDeleteNewImage(index)}
                    />
                  </Box>
                </Tooltip>
              </Flex>
            </GridItem>
          ))}
          <Flex align="start" justify="start">
            <Tooltip hasArrow label="Add images">
              <AddIcon onClick={() => setToggle(!toggle)} ml={4} />
            </Tooltip>
            {toggle && (
              <Input
                multiple
                id="image"
                name="image"              
                accept="image/png,image/jpeg"
                type="file"
                border="none"
                borderColor="none"
                onChange={handleImageChange}
              />
            )}
          </Flex>
        </SimpleGrid>

        <Input
          type="title"
          value={newPostData?.title}
          onChange={(e) =>
            setNewPostData({ ...newPostData, title: e.target.value })
          }
        />
        <ReactQuill
          className="h-full mb-16 w-full"
          id="story"
          type="text"
          name="story"
          value={newPostData.text}
          onChange={handleQuillChange}
        />
        <Button onClick={handleUpdate}>Submit</Button>
      </Stack>
    </Container>
  );
};

export default EditPost;
