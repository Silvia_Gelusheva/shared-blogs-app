import { Box, Container, Heading, Stack, Text } from "@chakra-ui/react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate, useParams } from "react-router-dom";

import ProfileButtons from "../components/Profile/ProfileButtons";
import ProfileInfo from "../components/Profile/ProfileInfo";
import PropTypes from "prop-types";
import { fetchUserProfile } from "../redux/actions/user-actions";
import { getPostByAuthor } from "../redux/features/postsSlice";
import { motion } from "framer-motion";
import { useEffect } from "react";

const Biography = ({ currentUser }) => {
  return (
    <Box mt={6}>
      <Heading size="md">About me</Heading>
      <Text color="gray" fontSize="sm">
        {currentUser?.bio || "No biography available."}
      </Text>
    </Box>
  );
};
Biography.propTypes = {
  currentUser: PropTypes.shape({
    bio: PropTypes.string,
  }),
};
const Profile = () => {
  const { displayName } = useParams();
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const authUser = useSelector((state) => state.user.user);
  const currentUser = useSelector((state) => state.profile.user);

  useEffect(() => {
    dispatch(fetchUserProfile(displayName));
  }, [dispatch, displayName]);
  const allPostsByAuthor = getPostByAuthor(currentUser?.displayName);
  const postsByAuthor = useSelector(allPostsByAuthor);

  return (
    <Container maxW="container.lg" mt={3}>
      <motion.div
        initial={{ opacity: 0 }}
        animate={{ opacity: 1 }}
        transition={{ duration: 1 }}
      >
        <ProfileInfo currentUser={currentUser} />

        <Stack direction="row" mt={6} spacing={4} justify="center">
          <ProfileButtons currentUser={currentUser} authUser={authUser} />
        </Stack>
        <Biography currentUser={currentUser} />
        <Stack mt={6}>
          <Heading size="lg">My Blogs</Heading>
          {postsByAuthor.toReversed().map((post) => (
            <motion.div
              key={post.postId}
              whileHover={{ scale: 1.05 }}
              whileTap={{ scale: 0.95 }}
            >
              <Box
                p={4}
                borderWidth="1px"
                borderRadius="lg"
                overflow="hidden"
                cursor="pointer"
                mt={4}
                onClick={() => navigate(`/${post.postId}`)}
              >
                <Heading size="sm" mb={2}>
                  {post.title}
                </Heading>
                <Text
                  fontSize="sm"
                  dangerouslySetInnerHTML={{
                    __html: post?.text?.substring(0, 250),
                  }}
                />
              </Box>
            </motion.div>
          ))}
        </Stack>
      </motion.div>
    </Container>
  );
};

export default Profile;
