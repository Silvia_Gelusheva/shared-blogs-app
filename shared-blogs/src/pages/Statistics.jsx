import {
  Box,
  Flex,
  Heading,
  Image,
  Text,
  VStack,
  useBreakpointValue,
} from "@chakra-ui/react";

import { motion } from "framer-motion";
import { useSelector } from "react-redux";

const Statistics = () => {
  const users = useSelector((state) => state.users.users);
  const posts = useSelector((state) => state.posts.posts);

  const adminsCount = useSelector(
    (state) => state.users.users.filter((user) => user.role === 2).length
  );

  const userRequestsCount = useSelector(
    (state) => state.requests.requests.length
  );

  const blockedPostsCount = useSelector(
    (state) => state.posts.posts.filter((post) => post.isBlocked).length
  );

  const reportedPostsCount = useSelector(
    (state) =>
      state.posts.posts.filter(
        (post) => post.reports && Object.keys(post.reports).length > 0
      ).length
  );

  const flexDirection = useBreakpointValue({ base: "column", md: "row" });
  const margin = useBreakpointValue({
    base: "0",
    sm: "8",
    md: "2",
    lg: "2",
    xl: "2",
  });

  return (
    <Box p="4">
      <motion.div
        initial={{ opacity: 0 }}
        animate={{ opacity: 1 }}
        transition={{ duration: 1 }}
      >
        <VStack
          align={{
            base: "center",
            sm: "center",
            md: "center",
            lg: "end",
          }}
          mb={8}
        >
          <Heading fontSize="3xl" fontWeight="bold">
            Hello, Admin!
          </Heading>
        </VStack>
        <Flex
          justify="space-between"
          p="4"
          flexWrap="wrap"
          flexDirection={flexDirection}
        >
          <Flex
            direction="column"
            p="4"
            borderWidth="1px"
            borderRadius="lg"
            boxShadow="lg"
            flex="1"
            margin={margin}
            mb={4}
            position="relative"
            align="center"
          >
            <Image
              src="https://cdn-icons-png.flaticon.com/512/164/164600.png"
              alt="User Image"
              w="50px"
              h="50px"
              borderRadius="full"
              position="absolute"
              top="-25px"
              left="50%"
              transform="translateX(-50%)"
            />
            <Text fontSize="lg" fontWeight="bold" my={4} textAlign="center">
              Users Stats
            </Text>
            <Flex fontSize="md" mb={2} gap={2} align="center">
              Total Users:{" "}
              <Text fontWeight="bold" fontSize="lg">
                {users.length}
              </Text>
            </Flex>
            <Flex fontSize="md" mb={2} gap={2} align="center">
              Admins:{" "}
              <Text fontWeight="bold" fontSize="lg">
                {adminsCount}
              </Text>
            </Flex>
            <Flex fontSize="md" mb={2} gap={2} align="center">
              User's Requests:{" "}
              <Text fontWeight="bold" fontSize="lg">
                {userRequestsCount}
              </Text>
            </Flex>
          </Flex>

          <Flex
            direction="column"
            p="4"
            borderWidth="1px"
            borderRadius="lg"
            boxShadow="lg"
            flex="1"
            margin={margin}
            mb={4}
            position="relative"
            align="center"
          >
            <Image
              src="https://cdn-icons-png.flaticon.com/512/4631/4631090.png"
              alt="Post Image"
              w="50px"
              h="50px"
              borderRadius="full"
              position="absolute"
              top="-25px"
              left="50%"
              transform="translateX(-50%)"
            />
            <Text fontSize="lg" fontWeight="bold" my={4}>
              Posts Stats
            </Text>
            <Flex fontSize="md" mb={2} gap={2} align="center">
              Total Posts:
              <Text fontWeight="bold" fontSize="lg">
                {posts.length}
              </Text>
            </Flex>
            <Flex fontSize="md" mb={2} gap={2} align="center">
              Blocked Posts:
              <Text fontWeight="bold" fontSize="lg">
                {blockedPostsCount}
              </Text>
            </Flex>
            <Flex fontSize="md" mb={2} gap={2} align="center">
              Reported Posts:
              <Text fontWeight="bold" fontSize="lg">
                {reportedPostsCount}
              </Text>
            </Flex>
          </Flex>
        </Flex>
      </motion.div>
    </Box>
  );
};

export default Statistics;
