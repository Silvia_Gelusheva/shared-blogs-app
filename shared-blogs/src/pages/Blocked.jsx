import {
  Badge,
  Box,
  Button,
  Link as ChakraLink,
  Flex,
  Heading,
  Image,
  Text,
  VStack,
} from "@chakra-ui/react";
import { useDispatch, useSelector } from "react-redux";

import { FaUnlockKeyhole } from "react-icons/fa6";
import { Link } from "react-router-dom";
import { toggleBlockPost } from "../redux/actions/post-actions";

const Blocked = () => {
  const dispatch = useDispatch();

  const handleUnblockPost = (post) => {
    try {
      dispatch(toggleBlockPost(post));
    } catch (error) {
      console.log(error.message);
    }
  };
  const posts = useSelector((state) => state.posts.posts);
  const blockedPostsCount = useSelector(
    (state) => state.posts.posts.filter((post) => post.isBlocked).length
  );
  return (
    <Box p="4">
      <VStack
        align={{
          base: "center",
          sm: "center",
          md: "center",
          lg: "end",
        }}
        mb={8}
      >
        <Heading fontSize="3xl" fontWeight="bold">
          Blocked Posts{" "}
          <Badge
            colorScheme="teal"
            ml="1"                
            rounded="full"
            px="2"
            py="1"
          >
            {blockedPostsCount}
          </Badge>
        </Heading>
      </VStack>
      {posts.map(
        (post) =>
          post?.isBlocked && (
            <Box
              key={post.postId}
              borderWidth="1px"
              borderRadius="md"
              boxShadow="md"
              p="4"
              mb={4}
            >
              <Flex direction={{ base: "column", md: "row" }} align="center">
                <Flex direction="column" align="center">
                  <Image
                    src={post?.urls ? post?.urls[0] : post?.url}
                    alt="Blog Image"
                    w={40}
                    mb={4}
                    borderRadius="lg"
                  />
                  <ChakraLink
                    as={Link}
                    color="blue.500"
                    to={`/${post?.postId}`}
                    textDecoration="underline"
                  >
                    <Text fontSize="sm" fontWeight="semibold">
                      ID: {post.postId}
                    </Text>
                  </ChakraLink>
                </Flex>
                <VStack
                  ml={{ base: 0, md: 4 }}
                  align="start"
                  flex="1"
                  mt={{ base: 2, md: 0 }}
                >
                  <Flex
                    justify="space-between"
                    gap={2}
                    w="full"
                    align="center"
                    direction={{
                      base: "column",
                      sm: "column",
                      md: "column",
                      lg: "row",
                      xl: "row",
                    }}
                  >
                    <Button
                      size="sm"
                      colorScheme="green"
                      onClick={() => handleUnblockPost(post)}
                      leftIcon={<FaUnlockKeyhole />}
                    >
                      Unblock
                    </Button>
                  </Flex>
                  <Text fontSize="md" color="gray.500" mt={1}>
                    Title: {post.title}
                  </Text>
                  <Text fontSize="md" color="gray.500">
                    Added on:{" "}
                    {new Date(post.addedOn).toLocaleDateString("en-GB")}
                  </Text>
                  <Text fontSize="md" color="gray.500">
                    Author: {post.author}
                  </Text>
                  {/* <Text
                    fontSize="sm"
                    dangerouslySetInnerHTML={{
                      __html: post?.text?.substring(0, 250),
                    }}
                  /> */}
                </VStack>
              </Flex>
            </Box>
          )
      )}
    </Box>
  );
};

export default Blocked;
