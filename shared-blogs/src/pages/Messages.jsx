import { Box, Button, Flex, List, ListItem, Textarea } from '@chakra-ui/react';
import { filteredUsers, selectAllUsers } from '../redux/features/usersSlice';
import { useDispatch, useSelector } from 'react-redux';
import { useEffect, useRef, useState } from 'react';

import SearchBar from '../components/SearchBar';
import { fetchAllUsers } from '../redux/actions/user-actions';

const Messages = () => {
  const [selectedUser, setSelectedUser] = useState(null);
  const inputRef = useRef("");
  const dispatch = useDispatch();

  const users = useSelector((state) => selectAllUsers(state));

  const filterUsers = () => {
    dispatch(filteredUsers(inputRef.current.value));
  };

  useEffect(() => {
    dispatch(fetchAllUsers());
  }, [dispatch]);

  const filteredUsersList = users ? users.filter((u) => !u.isBlocked) : [];

  const handleUserSelect = (user) => {
    setSelectedUser(user);
  };

  return (
    <Flex>
      <Box w="30%" p={4} borderRight="1px solid #ccc">
        <SearchBar handleChange={filterUsers} inputRef={inputRef} />
        <List spacing={2}>
          {filteredUsersList.map((user) => (
            <ListItem
              key={user.id}
              cursor="pointer"
              onClick={() => handleUserSelect(user)}
            >
              {user.username}
            </ListItem>
          ))}
        </List>
      </Box>
      <Box w="70%" p={4}>
        {selectedUser ? (
          <Box>
            <Box mb={4} fontWeight="bold">
              Chatting with {selectedUser.username}
            </Box>
            <Box>
              {/* Display messages exchanged between currentUser and selectedUser */}
            </Box>
            <Box mt={4}>
              <Textarea
                placeholder="Type your message..."
                value={""} // You need to handle the value and onChange
                onChange={() => {}} // You need to handle the value and onChange
              />
            </Box>
            <Button mt={2}>Send</Button>
          </Box>
        ) : (
          <Box textAlign="center">Select a user to start chatting</Box>
        )}
      </Box>
    </Flex>
  );
};

export default Messages;
