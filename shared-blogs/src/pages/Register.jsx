import {
  Avatar,
  Box,
  Button,
  Flex,
  FormControl,
  FormLabel,
  IconButton,
  Input,
  Text,
} from "@chakra-ui/react";
import { FaCamera, FaTrash } from "react-icons/fa";
import { Link, useNavigate } from "react-router-dom";
import { db, storage } from "../firebase/firebase.config";
import { get, ref } from "firebase/database";
import {
  getDownloadURL,
  ref as storageRef,
  uploadBytes,
} from "firebase/storage";
import { setError, setLoading } from "../redux/features/userSlice";

import { motion } from "framer-motion";
import { registerUser } from "../redux/actions/user-actions";
import { useDispatch } from "react-redux";
import { useState } from "react";

const Register = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [image, setImage] = useState(null);
  const [imageUrl, setImageUrl] = useState("");
  const [displayName, setDisplayName] = useState("");
  const [name, setName] = useState("");
  const [lastName, setLastName] = useState("");

  const handleAvatarChange = (e) => {
    const file = e.target.files[0];
    setImage(file);
    setImageUrl(URL.createObjectURL(file));
  };

  const handleAvatarClick = () => {
    document.getElementById("avatarInput").click();
  };

  const handleRemoveAvatar = () => {
    setImage(null);
    setImageUrl("");
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    if (
      name.length > 20 ||
      lastName.length > 20 ||
      name.length < 2 ||
      lastName.length < 2
    ) {
      alert(
        "Error: Name and last name should be more than 2 and less than 20 characters"
      );
      return;
    }
    if (password.length < 5 || password.length > 15) {
      alert(
        "Error: Password should be more than 5 and less than 10 characters"
      );
      return;
    }

    if (!email || email.trim() === "") {
      alert("Error: Email is required");
      return;
    }

    if (!/^\S+@\S+\.\S+$/.test(email)) {
      alert("Error: Invalid email format");
      return;
    }

    const displayNameRef = ref(db, `users/${displayName}`);
    const displayNameSnapshot = await get(displayNameRef);
    if (displayNameSnapshot.exists()) {
      alert("Username is already taken. Please choose another one.");
      return;
    }

    let avatarUrl = "";
    if (image) {
      const photoRef = storageRef(storage, `avatars/${displayName}/avatar`);
      const result = await uploadBytes(photoRef, image);
      avatarUrl = await getDownloadURL(result.ref);
    }

    dispatch(
      registerUser({
        joinedOn: Date.now(),
        email,
        password,
        displayName,
        name,
        lastName,
        url: avatarUrl,
        role: 1,
      })
    )
      .then((result) => {
        setLoading(false);
        setError(null);
        navigate("/");
      })
      .catch((error) => {
        setError(error.message);
        setLoading(false);
      });
  };

  return (
    <Flex width="full" height="full" justify="center">
      <motion.div
        initial={{ opacity: 0 }}
        animate={{ opacity: 1 }}
        transition={{ duration: 1 }}
      >
        <Box
          w="lg"
          // h="max-content"
          borderWidth="1px"
          borderRadius="lg"
          overflow="hidden"
          p={4}
          mt={2}
        >
          <FormControl>
            <Flex direction="column" gap={4} alignItems="center">
              <Flex direction="column" alignItems="center">
                <FormLabel>
                  Choose avatar
                  {image && (
                    <IconButton
                      icon={<FaTrash />}
                      aria-label="Remove Avatar"
                      variant="ghost"
                      onClick={handleRemoveAvatar}
                    />
                  )}
                </FormLabel>
                <Box position="relative">
                  <Avatar
                    src={imageUrl}
                    bg="teal"
                    size="2xl"
                    cursor="pointer"
                    onClick={handleAvatarClick}
                  />
                  <input
                    id="avatarInput"
                    type="file"
                    accept="image/png,image/jpeg"
                    onChange={handleAvatarChange}
                    style={{ display: "none" }}
                  />
                  <IconButton
                    position="absolute"
                    rounded="full"
                    bottom="0"
                    right="0"
                    icon={<FaCamera />}
                    colorScheme="gray"
                    size="sm"
                    onClick={handleAvatarClick}
                  />
                </Box>
              </Flex>
              <Input
                id="name"
                type="text"
                name="name"
                placeholder="Name"
                value={name}
                onChange={(e) => setName(e.target.value)}
              />
              <Input
                id="lastName"
                type="text"
                name="lastName"
                placeholder="Last Name"
                value={lastName}
                onChange={(e) => setLastName(e.target.value)}
              />
              <Input
                id="email"
                type="email"
                name="email"
                placeholder="Email"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              />
              <Input
                id="password"
                type="password"
                name="password"
                placeholder="Password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
              />
              <Input
                id="displayName"
                type="text"
                name="displayName"
                placeholder="Username"
                value={displayName}
                onChange={(e) => setDisplayName(e.target.value)}
              />
            </Flex>
            <Flex>
              <Button
                mt={4}
                bg="teal"
                w="full"
                type="submit"
                onClick={handleSubmit}
              >
                Submit
              </Button>
            </Flex>
          </FormControl>
          <Flex direction="column" align="center">
            <Text size="sm" mt={4}>
              Already have an account?
            </Text>
            <Link to="/">
              <Text color="teal" fontWeight="bold">
                Sign in
              </Text>
            </Link>
          </Flex>
        </Box>
      </motion.div>
    </Flex>
  );
};

export default Register;
