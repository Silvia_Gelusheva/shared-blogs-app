import { Box, Grid, Heading, VStack } from "@chakra-ui/react";
import {
  getSavedPostsByIds,
  removeFromSaved,
} from "../redux/actions/saved-actions";
import { useDispatch, useSelector } from "react-redux";
import { useEffect, useRef, useState } from "react";

import SavedBlogCard from "../components/cards/SavedBlogCard";
import SearchBar from "../components/SearchBar";
import { motion } from "framer-motion";

const Saved = () => {
  const [posts, setPosts] = useState([]);
  const [searchQuery, setSearchQuery] = useState("");
  const dispatch = useDispatch();
  const ids = useSelector((state) => state.saved.ids);
  const authUser = useSelector((state) => state.user.user);
  const inputRef = useRef(null); 

  useEffect(() => {
    getSavedPostsByIds(ids).then((res) => {
      const filteredPosts = [];
      ids.forEach((id, index) => {
        if (res[index] !== undefined && res[index] !== null) {
          filteredPosts.push(res[index]);
        } else {
          dispatch(removeFromSaved([id, authUser.displayName]));
        }
      });

      setPosts(filteredPosts);
    });
  }, [ids, authUser.displayName, dispatch]);

  const handleSearch = () => {
    setSearchQuery(inputRef.current.value); 
  };

  const filteredPosts = posts.filter((post) =>
    post.title.toLowerCase().includes(searchQuery.toLowerCase())
  );

  return (
    <Box px="10">
      <motion.div
        initial={{ opacity: 0 }}
        animate={{ opacity: 1 }}
        transition={{ duration: 1 }}
      >
        <VStack align="end" maxW="auto" justify="end" width="full" mb={10}>
          <Heading fontSize="3xl" fontWeight="bold">
            Saved Blogs
          </Heading>
          <SearchBar handleChange={handleSearch} inputRef={inputRef} />
        </VStack>
        <Grid gridGap="10" p={2}>
          {filteredPosts.map((post) => (
            <SavedBlogCard key={post?.postId} post={post} />
          ))}
        </Grid>
      </motion.div>
    </Box>
  );
};

export default Saved;
