import { Box, Drawer, DrawerContent, useDisclosure } from "@chakra-ui/react";

import { MobileNav } from "../components/Navigation/MobileNav";
import PropTypes from "prop-types";
import { SidebarContent } from "../components/Navigation/SideBarContent";

function SideBar({ children }) {
  const { isOpen, onOpen, onClose } = useDisclosure();

  return (
    <Box minH="100vh">
      <SidebarContent
        onClose={() => onClose}
        display={{ base: "none", md: "block" }}
      />
      <Drawer
        isOpen={isOpen}
        placement="left"
        onClose={onClose}
        returnFocusOnClose={false}
        onOverlayClick={onClose}
        size="full"
      >
        <DrawerContent>
          <SidebarContent onClose={onClose} />
        </DrawerContent>
      </Drawer>
      {/* mobilenav */}
      <MobileNav onOpen={onOpen} />
      <Box ml={{ base: 0, md: 60 }} p="4">
        {/* Content */}
        {children}
      </Box>
    </Box>
  );
}
SideBar.propTypes = {
  children: PropTypes.node,
};

export default SideBar;
