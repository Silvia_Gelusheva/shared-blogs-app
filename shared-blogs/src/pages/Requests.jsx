import {
  Badge,
  Box,
  Button,
  Flex,
  Heading,
  Text,
  VStack,
} from "@chakra-ui/react";
import {
  deletePostAsyncThunk,
  dismissReport,
} from "../redux/actions/post-actions";
import { deleteUserAsyncThunk, makeAdmin } from "../redux/actions/user-actions";
import {
  dismissRequest,
  fetchAllRequests,
} from "../redux/actions/requests-actions";
import { useDispatch, useSelector } from "react-redux";

import AdminMessages from "../components/modals/requests/AdminMessages";
import { deleteSavedPostByUser } from "../redux/actions/saved-actions";
import { useEffect } from "react";

const Requests = () => {
  const dispatch = useDispatch();
  const requests = useSelector((state) => state.requests.requests);

  const requestsCount = useSelector((state) => state.requests.requests.length);
  const posts = useSelector((state) => state.posts.posts);

  useEffect(() => {
    dispatch(fetchAllRequests());
  }, [dispatch]);

  function getPostsByDisplayName(posts, displayName) {
    const matchingPosts = {};

    Object.entries(posts).forEach(([key, post]) => {
      if (post.author === displayName) {
        matchingPosts[key] = post;
      }
    });

    return Object.values(matchingPosts);
  }

  const handleDeleteProfile = async (request, posts, displayName) => {
    try {
      const delPosts = await getPostsByDisplayName(posts, displayName);
      await Promise.all(
        delPosts.map(async (post) => {
          if (post.reports) {
            await Promise.all(
              Object.keys(post.reports).map(async (key) => {
                await dispatch(dismissReport([displayName, key, post.postId]));
              })
            );
          }
          await dispatch(deletePostAsyncThunk([post.postId, displayName]));
        })
      );

      await Promise.all([
        dispatch(deleteSavedPostByUser(displayName)),
        dispatch(deleteUserAsyncThunk(displayName)),
        dispatch(dismissRequest(request)),
      ]);
    } catch (error) {
      console.error(error.message);
    }
  };

  const handleDismissRequest = async (request) => {
    await dispatch(dismissRequest(request));
    dispatch(fetchAllRequests());
  };


  const handleMakeAdmin = async (request) => {
    await dispatch(makeAdmin(request?.displayName));
    // await dispatch(dismissRequest(request));
    dispatch(fetchAllRequests());
  };

  return (
    <Box p="4">
      <VStack
        align={{
          base: "center",
          sm: "center",
          md: "center",
          lg: "end",
        }}
        mb={8}
      >
        <Heading fontSize="3xl" fontWeight="bold">
          Requests
          <Badge colorScheme="teal" ml="2" rounded="full" px="2" py="1">
            {requestsCount}
          </Badge>
        </Heading>
      </VStack>

      {requests &&
        Object.values(requests).map((request, index) => (
          <Box
            key={index}
            borderWidth="1px"
            borderRadius="md"
            boxShadow="md"
            p="4"
            mb={4}
          >
            <Flex
              justify="space-between"
              gap={2}
              w="full"
              align="center"
              mb={4}
              direction={{
                base: "column",
                sm: "column",
                md: "column",
                lg: "row",
                xl: "row",
              }}
            >
              <Text fontSize="lg" fontWeight="semibold">
                User: {request?.displayName}
              </Text>
              <Flex gap={2}>
                <AdminMessages request={request} />
                <Button
                  colorScheme="red"
                  size="sm"
                  onClick={() => handleDismissRequest(request)}
                >
                  Dismiss
                </Button>
              </Flex>
            </Flex>

            <Flex
              gap={2}
              w="full"
              align="start"
              direction={{
                base: "column",
                sm: "column",
                md: "column",
                lg: "row",
                xl: "row",
              }}
            >
              {request?.confirmPromoteProduct && (
                <Button
                  colorScheme="blue"
                  size="xs"
                  onClick={() => alert("TO DO")}
                >
                  Confirm Promote Product
                </Button>
              )}
              {request?.confirmPromoteEvent && (
                <Button
                  colorScheme="green"
                  size="xs"
                  onClick={() => alert("TO DO!")}
                >
                  Confirm Promote Event
                </Button>
              )}
              {request.confirmAdmin && (
                <Button
                  colorScheme="yellow"
                  size="xs"
                  onClick={() => handleMakeAdmin(request)}
                >
                  Make Admin
                </Button>
              )}
              {request.confirmDelete && (
                <Button
                  colorScheme="red"
                  size="xs"
                  onClick={() =>
                    handleDeleteProfile(request, posts, request?.displayName)
                  }
                >
                  Delete User
                </Button>
              )}
            </Flex>

            <Text mt={4}>{request.text}</Text>
          </Box>
        ))}
    </Box>
  );
};

export default Requests;
