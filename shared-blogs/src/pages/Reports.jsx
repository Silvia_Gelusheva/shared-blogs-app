import {
  Badge,
  Box,
  Button,
  Flex,
  Heading,
  IconButton,
  Text,
  VStack,
  useBreakpointValue,
} from "@chakra-ui/react";
import {
  dismissAllPostReports,
  dismissReport,
  fetchAllReports,
} from "../redux/actions/post-actions";
import { useDispatch, useSelector } from "react-redux";

import { Link as ChakraLink } from "@chakra-ui/react";
import { CloseIcon } from "@chakra-ui/icons";
import { Link } from "react-router-dom";
import { useEffect } from "react";

const Reported = () => {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(fetchAllReports());
  }, [dispatch]);
  const reports = useSelector((state) => state.posts.reports);

  const reportedPostsCount = useSelector((state) =>
  state.posts.posts.filter((post) => post.reports && Object.keys(post.reports).length > 0).length
);

  const handleDismissReport = async (postId, reportId, displayName) => {
    try {
      dispatch(dismissReport([postId, reportId, displayName]));
      dispatch(fetchAllReports());
    } catch (error) {
      console.log(error.message);
    }
  };

  const handleDismissAllReportsByPost = async (postId) => {
    try {
      dispatch(dismissAllPostReports(postId));
      dispatch(fetchAllReports());
    } catch (error) {
      console.log(error.message);
    }
  };
  const showDivider = useBreakpointValue({
    base: false,
    xs: false,
    sm: false,
    md: true,
    lg: true,
    xl: true,
  });
  return (
    <Box p="4" >
         <VStack
        align={{
          base: "center",
          sm: "center",
          md: "center",
          lg: "end",
        }}
        mb={8}
      >
        <Heading fontSize="3xl" fontWeight="bold">
          Reported Blogs
          <Badge
            colorScheme="teal"
            ml="2"                
            rounded="full"
            px="2"
            py="1"
          >
            {reportedPostsCount}
          </Badge>
        </Heading>
      </VStack>
      {reports &&
        Object.entries(reports).map(([postId, report]) => (
          <VStack
            key={postId}
            align="start"
            spacing={6}
            borderWidth="1px"
            p="4"
            borderRadius="md"
            boxShadow="md"
            width="100%"
            marginX="auto"
            mb={4}
          >
            <Flex
              justify="space-between"
              gap={2}
              w="full"
              align="start"
              direction={{
                base: "column",
                sm: "column",
                md: "column",
                lg: "row",
                xl: "row",
              }}
            >
              <Flex
                direction={{
                  base: "column",
                  sm: "column",
                  md: "column",
                  lg: "row",
                  xl: "row",
                }}
                gap={2}
                justify="end"
              >    
                <ChakraLink
                  as={Link}
                  color="blue.500"
                  to={`/${postId}`}
                  textDecoration="underline"
                >
                  <Text fontSize="sm" fontWeight="semibold">
                  ID: {postId}
                  </Text>
                </ChakraLink>
              </Flex>

              <Box>
                <Button
                  size="sm"
                  colorScheme="teal"
                  onClick={() => handleDismissAllReportsByPost(postId)}
                >
                  Dismiss All
                </Button>
              </Box>
            </Flex>
            {Object.values(report).map((rep) => (
              <Box
                key={rep?.reportId}
                p="4"
                borderWidth="1px"
                borderRadius="md"
                boxShadow="md"
                width="100%"
              >
                <Flex justify="space-between" align="center" mb="2">
                  <Flex
                    direction={{
                      base: "column",
                      lg: "row",
                      md: "row",
                      sm: "column",
                    }}
                  >
                    <Text fontWeight="bold">{rep?.reportedBy}</Text>
                    {showDivider && (
                      <Text mb="2" mx="2">
                        |
                      </Text>
                    )}
                    <Text mb="2">{rep?.selectedArguments?.join(" | ")}</Text>
                  </Flex>
                  <IconButton
                    size="xs"
                    colorScheme="red"
                    aria-label="Dismiss Report"
                    icon={<CloseIcon />}
                    onClick={() =>
                      handleDismissReport(
                        rep.postId,
                        rep.reportId,
                        rep.reportedBy
                      )
                    }
                  />
                </Flex>

                <Text fontSize="sm" color="gray.600" noOfLines={2}>
                  {rep?.message || "No message"}
                </Text>
              </Box>
            ))}
          </VStack>
        ))}
    </Box>
  );
};

export default Reported;
