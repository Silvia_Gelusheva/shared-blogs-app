import {
  Button,
  IconButton,
  Menu,
  MenuButton,
  Popover,
  PopoverArrow,
  PopoverBody,
  PopoverCloseButton,
  PopoverContent,
  PopoverHeader,
  PopoverTrigger,
  Stack,
  Text,
} from "@chakra-ui/react";
import {
  fetchAllSavedPostsByUser,
  removeFromSaved,
  savePost,
} from "../redux/actions/saved-actions";
import { useDispatch, useSelector } from "react-redux";

import { HamburgerIcon } from "@chakra-ui/icons";
import PropTypes from "prop-types";
import QuickView from "../components/modals/QuickView";
import ReportForm from "../components/modals/ReportForm";
import { deletePostAsyncThunk } from "../redux/actions/post-actions";
import { useEffect } from "react";

const DropdownMenu = ({ post }) => {
  const dispatch = useDispatch();
  const authUser = useSelector((state) => state.user.user);

  useEffect(() => {
    dispatch(fetchAllSavedPostsByUser(authUser?.displayName));
  }, [dispatch, authUser?.displayName]);

  const isFavorite = useSelector((state) =>
    state?.saved?.ids?.includes(post?.postId)
  );

  const toggleFavorite = async () => {
    if (isFavorite) {
      await dispatch(removeFromSaved([post?.postId, authUser?.displayName]));
    } else {
      await dispatch(savePost([post?.postId, authUser?.displayName]));
    }
  };

  const isReported =
    post?.reports &&
    Object.values(post?.reports)
      .map((report) => report?.reportedBy)
      .includes(authUser?.displayName);

  const handleDeletePost = (postId, displayName) => {
    dispatch(deletePostAsyncThunk([postId, displayName]));
  };

  return (
    <Menu>
      <Popover placement="right">
        <PopoverTrigger>
          <MenuButton
            as={IconButton}
            aria-label="Options"
            icon={<HamburgerIcon />}
            variant="outline"
          />
        </PopoverTrigger>
        <PopoverContent>
          <PopoverArrow />
          <PopoverCloseButton />
          <PopoverHeader>Select Option</PopoverHeader>
          <PopoverBody>
            <Stack direction="column" spacing={2}>
              <QuickView post={post} />
              {authUser?.displayName !== post?.author && (
                <Button variant="ghost" onClick={toggleFavorite}>
                  {isFavorite ? "Unsave" : "Save"}
                </Button>
              )}
              {authUser?.displayName === post?.author && (
                <Button
                  variant="ghost"
                  onClick={() => handleDeletePost(post?.postId, post?.author)}
                >
                  Delete
                </Button>
              )}
              {authUser?.displayName !== post?.author && !isReported && (
                <ReportForm post={post} isInDropdown={true} />
              )}
              {authUser?.displayName !== post?.author && isReported && (
                <Text textAlign="center" color="red.500" variant="ghost">
                  Reported
                </Text>
              )}
            </Stack>
          </PopoverBody>
        </PopoverContent>
      </Popover>
    </Menu>
  );
};

DropdownMenu.propTypes = {
  post: PropTypes.shape({
    postId: PropTypes.string.isRequired,
    author: PropTypes.string.isRequired,
    reports: PropTypes.object,
  }).isRequired,
};

export default DropdownMenu;
