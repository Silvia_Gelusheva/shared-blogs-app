import "react-responsive-carousel/lib/styles/carousel.min.css";

import { Container, Divider, Stack } from "@chakra-ui/react";
import {
  deletePostAsyncThunk,
  fetchPost,
  getPostByPostId,
  toggleBlockPost,
  toggleLikePost,
} from "../redux/actions/post-actions";
import {
  fetchAllSavedPostsByUser,
  removeFromSaved,
  savePost,
} from "../redux/actions/saved-actions";
import { useDispatch, useSelector } from "react-redux";
import { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";

import PostButtons from "../components/post/PostButtons";
import PostContent from "../components/post/PostContent";
import PostImages from "../components/post/PostImages";
import { getUserByName } from "../redux/actions/user-actions";
import { motion } from "framer-motion";

const Post = () => {
  const { postId } = useParams();
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const posts = useSelector((state) => state.posts.posts);
  const authUser = useSelector((state) => state.user.user);

  const [post, setPost] = useState({});
  const [currentUser, setCurrentUser] = useState({});

  useEffect(() => {
    getPostByPostId(postId).then((res) => setPost(res));
    getUserByName(authUser?.displayName).then((res) => setCurrentUser(res));
  }, [authUser, posts]);

  useEffect(() => {
    dispatch(fetchPost(postId));
    dispatch(fetchAllSavedPostsByUser(authUser?.displayName));
  }, [dispatch]);

  const isLiked = useSelector((state) => {
    const post = state.posts.posts.find((post) => post?.postId === postId);
    if (post && post?.likedBy) {
      return post.likedBy[authUser?.displayName] === true;
    }

    return false;
  });

  const isFavorite = useSelector((state) =>
    state?.saved?.ids?.includes(post?.postId)
  );

  const toggleFavorite = async () => {
    if (isFavorite) {
      await dispatch(removeFromSaved([post?.postId, authUser?.displayName]));
    } else {
      await dispatch(savePost([post?.postId, authUser?.displayName]));
    }
  };

  const toggleLikes = async () => {
    try {
      await dispatch(toggleLikePost([post?.postId, authUser?.displayName]));
    } catch (error) {
      console.error("Error toggling likes:", error.message);
    }
  };

  const handleBlockPost = async () => {
    try {
      await dispatch(toggleBlockPost(post));
    } catch (error) {
      console.error("Error toggling block:", error.message);
    }
  };

  const handleDeletePost = async () => {
    try {
      await dispatch(deletePostAsyncThunk([post?.postId, post?.author]));
      navigate(-1);
    } catch (error) {
      console.error("Error toggling block:", error.message);
    }
  };

  const isReported = useSelector((state) => {
    const post = state.posts.posts.find((post) => post?.postId === postId);
    if (post && post?.reports) {
      return Object.values(post?.reports)
        .map((report) => report?.reportedBy)
        .includes(authUser?.displayName);
    }
  });

  return (
    <Container maxW={"5xl"} mt={4}>
      <motion.div
        initial={{ opacity: 0 }}
        animate={{ opacity: 1 }}
        transition={{ duration: 1 }}
      >
        <Stack spacing={{ base: 2, md: 4 }} py={{ base: 5, md: 4 }}>
          <PostButtons
            authUser={authUser}
            currentUser={currentUser}
            post={post}
            toggleLikes={toggleLikes}
            toggleFavorite={toggleFavorite}
            isFavorite={isFavorite}
            isLiked={isLiked}
            isReported={isReported}
            handleBlockPost={handleBlockPost}
            handleDeletePost={handleDeletePost}
          />
          <Divider borderColor="gray.300" my={4} />
          <PostContent post={post} />
          <PostImages post={post} />
        </Stack>
      </motion.div>
    </Container>
  );
};

export default Post;
