import {
  Box,
  Divider,
  Flex,
  Heading,
  SimpleGrid,
  Text,
  VStack,
} from "@chakra-ui/react";
import { filteredPosts, selectAllPosts } from "../redux/features/postsSlice";
import { useDispatch, useSelector } from "react-redux";
import { useEffect, useRef, useState } from "react";

import BlogCard from "../components/cards/BlogCard";
import Pagination from "../components/Pagination";
import PostForm from "../components/modals/PostForm";
import SearchBar from "../components/SearchBar";
import TrendingBlogCard from "../components/cards/TrendingBlogCard";
import { fetchAllPosts } from "../redux/actions/post-actions";
import { motion } from "framer-motion";

const Home = () => {
  const posts = useSelector((state) => selectAllPosts(state));
  const dispatch = useDispatch();
  const inputRef = useRef("");

  useEffect(() => {
    dispatch(fetchAllPosts());
  }, [dispatch]);

  const filterPosts = () => {
    dispatch(filteredPosts(inputRef.current.value));
  };

  const [currentPage, setCurrentPage] = useState(1);
  const postsPerPage = 3;
  const filteredAndReversedPosts = posts ? [...posts].filter((p) => !p.isBlocked).reverse() : [];
  const indexOfLastPost = currentPage * postsPerPage;
  const indexOfFirstPost = indexOfLastPost - postsPerPage;
  const currentPosts = filteredAndReversedPosts?.slice(indexOfFirstPost, indexOfLastPost);

  const totalPages = Math.ceil(filteredAndReversedPosts?.length / postsPerPage);

  const onPageChange = (page) => {
    setCurrentPage(page);
  };

  const getTopTwoPosts = () => {
    const sortedPosts = posts.slice().sort((a, b) => {
      const likesA = Object.keys(a?.likedBy || {}).length;
      const likesB = Object.keys(b?.likedBy || {}).length;
      return likesB - likesA;
    });
    return sortedPosts.slice(0, 3);
  };

  const topTwoPosts = getTopTwoPosts();

  return (
    <Flex direction="column" align="center" p={4}>
           <motion.div
        initial={{ opacity: 0 }}
        animate={{ opacity: 1 }}
        transition={{ duration: 1 }}
      >
      <Box mb={4}>
        <SearchBar handleChange={filterPosts} inputRef={inputRef} />
      </Box>

      <VStack spacing={4} w="full">
        <Heading as="h1" size="2xl">
          Welcome to Shared Blogs Community!
        </Heading>
        <Text fontSize="lg" color="gray.600">
          Explore and share your thoughts with others.
        </Text>

        <PostForm />
      </VStack>

      <Divider my={8} />

      <Flex w="full">
        <Pagination
          currentPage={currentPage}
          totalPages={totalPages}
          onPageChange={onPageChange}
        />
      </Flex>

      <SimpleGrid columns={{ base: 1, md: 2, lg: 3 }} spacing={8} w="full">
        {currentPosts.map((post) => (
          <BlogCard key={post?.postId} post={post} />
        ))}
      </SimpleGrid>

      <Divider my={8} />

      <VStack spacing={4} w="full">
        <Heading as="h2" size="xl">
          Trending Blogs
        </Heading>

        <SimpleGrid columns={{ base: 1, md: 2, lg: 3 }} spacing={4} w="full">
          {topTwoPosts.length > 0 ? (
            topTwoPosts.map(
              (post) =>
                !post.isBlocked && post.likedBy &&
                Object.keys(post.likedBy).length > 0 && (
                  <TrendingBlogCard key={post.postId} post={post} />
                )
            )
          ) : (
            <p>No posts available.</p>
          )}
        </SimpleGrid>
      </VStack>
      </motion.div>
    </Flex>
  );
};

export default Home;
