import { Route, Routes } from "react-router-dom";
import { fetchAllUsers, fetchUser } from "./redux/actions/user-actions";
import { useDispatch, useSelector } from "react-redux";
import { useEffect, useState } from "react";

import Blocked from "./pages/Blocked";
import Dashboard from "./pages/DashBoard";
import EditPost from "./pages/EditPost";
import EditUser from "./pages/EditUser";
import Home from "./pages/Home";
import LogIn from "./pages/LogIn";
import Messages from "./pages/Messages";
import Post from "./pages/Post";
import Profile from "./pages/Profile";
import Register from "./pages/Register";
import Reports from "./pages/Reports";
import Requests from "./pages/Requests";
import Saved from "./pages/Saved";
import SideBar from "./pages/SideBar";
import { Spinner } from "@chakra-ui/react";
import Users from "./pages/Users";
import { auth } from "./firebase/firebase.config";
import { fetchAllPosts } from "./redux/actions/post-actions";
import { onAuthStateChanged } from "firebase/auth";
import { setUser } from "./redux/features/userSlice";

function App() {
  const dispatch = useDispatch();
  const [authStatus, setAuthStatus] = useState("loading");
  const LoadingIcon = () => {
    return <Spinner />;
  };
  const isLoading = useSelector((state) => state.user.loading);
  useEffect(() => {
    const unsubscribe = onAuthStateChanged(auth, (user) => {
      if (user) {
        dispatch(fetchUser(user?.displayName));
        dispatch(fetchAllPosts());
        dispatch(fetchAllUsers());
        setAuthStatus("authenticated");
      } else {
        dispatch(setUser(null));
        setAuthStatus("unauthenticated");
      }
    });
    return () => unsubscribe();
  }, [dispatch]);

  return (
    <SideBar>
      {isLoading && <LoadingIcon />}
      {authStatus === "unauthenticated" && (
        <Routes>
          <Route path="/" element={<LogIn />} />
          <Route path="/register" element={<Register />} />
        </Routes>
      )}
      {authStatus === "authenticated" && (
        <Routes>
          <Route path="/" element={<Home />} />

          <Route path="/:postId" element={<Post />} />
          <Route path="/:postId/edit-post" element={<EditPost />} />
          <Route path="/users" element={<Users />} />
          <Route path="/users/:displayName" element={<Profile />} />
          <Route path="/users/:displayName/edit-user" element={<EditUser />} />
          <Route path="/messages" element={<Messages />} />
          <Route path="/favorites" element={<Saved />} />
          <Route path="/reported" element={<Reports />} />
          <Route path="/blocked" element={<Blocked />} />
          <Route path="/requests" element={<Requests />} />
          <Route path="/dashboard" element={<Dashboard />} />
        </Routes>
      )}
    </SideBar>
  );
}

export default App;
