import {
  Box,
  CloseButton,
  Divider,
  Flex,
  Text,
  Tooltip,
  useColorMode,
  useColorModeValue,
} from "@chakra-ui/react";
import { FaMoon, FaRegStar, FaSun } from "react-icons/fa";
import { FiHome, FiMessageSquare } from "react-icons/fi";
import { Link, useNavigate } from "react-router-dom";
import { selectUser, setUser } from "../../redux/features/userSlice";
import { useDispatch, useSelector } from "react-redux";

import { BsPeople } from "react-icons/bs";
import { CgLogOut } from "react-icons/cg";
import { DiYeoman } from "react-icons/di";
import { NavItem } from "./NavItem";
import { TbLayoutDashboard } from "react-icons/tb";
import { auth } from "../../firebase/firebase.config";
import { logOutUser } from "../../redux/actions/user-actions";
import { signOut } from "firebase/auth";

export const SidebarContent = ({ onClose, ...rest }) => {
  const { colorMode, toggleColorMode } = useColorMode();
  const isDark = colorMode === "dark";
  const currentUser = useSelector((state) => selectUser(state));
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const handleSignOut = () => {
    signOut(auth)
      .then(dispatch(logOutUser()))
      .then(setUser(null))
      .then(() => {
        navigate("/");
      })
      .catch((error) => {
        alert(error.message);
      });
  };
  return (
    <Box
      transition="3s ease"
      bg={useColorModeValue("white", "gray.900")}
      borderRight="1px"
      borderRightColor={useColorModeValue("gray.200", "gray.700")}
      w={{ base: "full", md: 60 }}
      pos="fixed"
      h="full"
      {...rest}
    >
      <Flex h="20" alignItems="center" mx="8" justifyContent="space-between">
        <Flex justify="center" align="center" gap="4">
          <Text fontSize="xl" fontFamily="monospace" fontWeight="bold">
            Shared Blogs
          </Text>
          <Tooltip label="Switch Themes" placement="top">
            <Flex>
              {!isDark ? (
                <FaMoon
                  onClick={toggleColorMode}
                  color="#ECC94B"
                  cursor="pointer"
                />
              ) : (
                <FaSun
                  onClick={toggleColorMode}
                  color="#D69E2E"
                  size={20}
                  cursor="pointer"
                />
              )}
            </Flex>
          </Tooltip>
        </Flex>
        <CloseButton display={{ base: "flex", md: "none" }} onClick={onClose} />
      </Flex>
      {currentUser && (
        <NavItem icon={CgLogOut}>
          <button onClick={handleSignOut}>Sing out</button>
        </NavItem>
      )}
      {currentUser && (
        <Link to="/">
          <NavItem icon={FiHome} onClick={onClose}>
            Home
          </NavItem>
        </Link>
      )}
      {currentUser && (
        <Link to={`/users/${currentUser?.displayName}`}>
          <NavItem icon={DiYeoman} onClick={onClose}>
            Profile
          </NavItem>
        </Link>
      )}
      {currentUser && (
        <Link to="/messages">
          <NavItem icon={FiMessageSquare} onClick={onClose}>
            Messages
          </NavItem>
        </Link>
      )}
      {currentUser && (
        <Link to="/favorites">
          <NavItem icon={FaRegStar} onClick={onClose}>
            Saved
          </NavItem>
        </Link>
      )}
      {currentUser && (
        <Link to="/users">
          <NavItem icon={BsPeople} onClick={onClose}>
            Community
          </NavItem>
        </Link>
      )}
      {currentUser && currentUser?.role === 2 && (
        <>
          <Divider my="4" borderColor="gray.300" />
          <Text px="4" color="gray.500" fontSize="sm" fontWeight="bold">
            Admin
          </Text>
          <Link to="/dashboard">
            <NavItem icon={TbLayoutDashboard} onClick={onClose}>
              Dashboard
            </NavItem>
          </Link>
        </>
      )}
    </Box>
  );
};
