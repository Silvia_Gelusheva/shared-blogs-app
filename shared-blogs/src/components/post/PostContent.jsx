import { Heading, Text } from "@chakra-ui/react"

const PostContent = ({post}) => {
  return (
    <>
        <Heading
          fontWeight={700}
          fontSize={{ base: "xl", sm: "2xl", md: "3xl" }}
        >
          {post?.title}
        </Heading>

        <Text fontStyle="italic" fontSize={{ base: "sm", md: "md" }}>
          by {post?.author}
        </Text>

        <Text
          fontSize={{ base: "sm", sm: "md", md: "sm" }}
          dangerouslySetInnerHTML={{ __html: post?.text }}
        />
    </>
  )
}

export default PostContent
