import { Box, Button, Flex } from "@chakra-ui/react";

import { FaArrowLeft } from "react-icons/fa";
import { FaLock } from "react-icons/fa";
import { FaRegEdit } from "react-icons/fa";
import ReportForm from "../modals/ReportForm";
import { useNavigate } from "react-router-dom";

const PostButtons = ({
  authUser,
  currentUser,
  post,
  toggleLikes,
  toggleFavorite,
  isFavorite,
  isLiked,
  isReported,
  handleBlockPost,
  handleDeletePost,
}) => {
  const navigate = useNavigate();
  return (
    <Flex
      justify={{ base: "space-between", sm: "center", md: "space-between" }}
      align="center"
      spacing={1}
    >
      <Box display="flex" alignItems="center">
        <Button
          leftIcon={<FaArrowLeft />}
          colorScheme="gray"
          size="sm"
          mr="2"
          onClick={() => navigate(-1)}
        >
          Back
        </Button>
        {authUser?.displayName === post?.author && (
          <Button
            leftIcon={<FaRegEdit />}
            colorScheme="gray"
            size="sm"
            onClick={() => navigate(`/${post?.postId}/edit-post`)}
          >
            Edit
          </Button>
        )}
      </Box>
      <Box display="flex" alignItems="center">
        {authUser?.displayName !== post?.author && (
          <Button
            onClick={toggleLikes}
            colorScheme={isLiked ? "gray" : "gray"}
            size="sm"
            mr="2"
          >
            {isLiked ? "❤️ Liked" : "♥ Like"}
          </Button>
        )}
        {authUser?.displayName !== post?.author && (
          <Button
            onClick={toggleFavorite}
            size="sm"
            mr={6}
            colorScheme={isFavorite ? "gray" : "gray"}
          >
            {isFavorite ? "⭐ Saved" : "★ Save"}
          </Button>
        )}
        {authUser?.displayName === post?.author && (
          <Button size="sm" colorScheme="gray" onClick={handleDeletePost}>
            🗑️ Delete
          </Button>
        )}
        {(currentUser?.role === 2 ||
          authUser?.displayName !== post?.author) && (
          <Box  ml="2">
            {isReported ? (
                 <Button size="sm">🚨 Reported</Button>           
            ) : (
              <ReportForm post={post} isInDropdown={false}/>  
            )}
          </Box>
        )}
        {currentUser?.role === 2 && (
          <Box onClick={handleBlockPost} ml="2">
            {post?.isBlocked ? (
              <FaLock size={32} color="crimson" cursor="pointer" />
            ) : (
              <Button size="sm">🔒 Block</Button>
            )}
          </Box>
        )}
      </Box>
    </Flex>
  );
};

export default PostButtons;
