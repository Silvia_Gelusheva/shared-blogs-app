import { Box } from "@chakra-ui/react"
import { Carousel } from "react-responsive-carousel"

const PostImages = ({post}) => {
  return (
    <Box align="center" mt={4} overflow="hidden">
          <Carousel infiniteLoop dynamicHeight width="500px">
            {post?.urls?.map((url, index) => (
              <img
                key={index}
                src={url}
                alt={`${post.title} - Image ${index + 1}`}
                height="300px"
                width="300px"
                style={{ border: "1px solid #ddd", borderRadius: "8px" }}
              />
            ))}
          </Carousel>
        </Box>
  )
}

export default PostImages
