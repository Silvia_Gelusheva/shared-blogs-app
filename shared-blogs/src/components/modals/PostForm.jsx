import "react-quill/dist/quill.snow.css";

import {
  Button,
  Flex,
  FormControl,
  GridItem,
  Input,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  SimpleGrid,
  VStack,
  useDisclosure,
} from "@chakra-ui/react";
import { getDownloadURL, uploadBytes } from "firebase/storage";
import { useDispatch, useSelector } from "react-redux";
import { useRef, useState } from "react";

import { FaPen } from "react-icons/fa";
import ReactQuill from "react-quill";
import { RiChatDeleteFill } from "react-icons/ri";
import { addPostToFirebase } from "../../redux/actions/post-actions";
import { storage } from "../../firebase/firebase.config";
import { ref as storageRef } from "firebase/storage";
import { v4 } from "uuid";

function PostForm() {
  const OverlayOne = () => (
    <ModalOverlay
      bg="blackAlpha.300"
      backdropFilter="blur(10px) hue-rotate(90deg)"
    />
  );
  const [overlay, setOverlay] = useState(<OverlayOne />);
  const initialRef = useRef(null);
  const finalRef = useRef(null);
  const { isOpen, onOpen, onClose } = useDisclosure();

  const dispatch = useDispatch();

  const [title, setTitle] = useState("");
  const [text, setText] = useState("");
  const [images, setImages] = useState([]);
  const [imagePreviews, setImagePreviews] = useState([]);
  const currentAuthor = useSelector((state) => state.user.user);

  const handleImageChange = (e) => {
    const selectedImages = Array.from(e.target.files);
    setImages((prevImages) => [...prevImages, ...selectedImages]);
    const previews = selectedImages.map((image) => URL.createObjectURL(image));
    setImagePreviews((prevPreviews) => [...prevPreviews, ...previews]);
  };

  const handleRemoveImage = (index) => {
    const updatedImages = [...images];
    updatedImages.splice(index, 1);

    const updatedPreviews = [...imagePreviews];
    updatedPreviews.splice(index, 1);

    setImages(updatedImages);
    setImagePreviews(updatedPreviews);
  };

  const handleSubmitForm = async (e) => {
    e.preventDefault();
    try {
      const promises = images.map(async (image) => {
        const storeRef = storageRef(
          storage,
          `posts/${currentAuthor.displayName}/${v4()}`
        );
        const result = await uploadBytes(storeRef, image);
        const url = await getDownloadURL(result.ref);
        return url;
      });

      Promise.all(promises).then((urls) => {
        let newPost = {
          title,
          text,
          urls,
          addedOn: Date.now(),
          author: currentAuthor.displayName,
        };

        dispatch(addPostToFirebase(newPost));
        setTitle("");
        setText("");
        setImages([]);
        setImagePreviews([]);
        onClose();
      });
    } catch (error) {
      console.log("error", error.message);
    }
  };

  return (
    <>
      <Flex>
        <Button
          leftIcon={<FaPen />}
          colorScheme="teal"
          cursor="pointer"
          _hover={{ color: "teal" }}
          onClick={() => {
            setOverlay(<OverlayOne />);
            onOpen();
          }}
        >
          Write a Blog
        </Button>
      </Flex>

      <Modal
        initialFocusRef={initialRef}
        finalFocusRef={finalRef}
        isOpen={isOpen}
        onClose={onClose}
        size="4xl"
      >
        {overlay}
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>Share post</ModalHeader>
          <ModalCloseButton />
          <ModalBody pb={6}>
            <FormControl isRequired>
              <VStack direction="column" gap={4} align="center">
                <Input
                  id="title"
                  type="text"
                  name="title"
                  placeholder="Title"
                  value={title}
                  onChange={(e) => setTitle(e.target.value)}
                />
                <ReactQuill
                  className="h-36 mb-16 w-full"
                  id="story"
                  type="text"
                  name="story"             
                  value={text}
                  onChange={setText}
                  placeholder="Story"
                />

                <Input
                  multiple
                  id="image"
                  name="image"
                  label="Image"
                  placeholder="Choose image"
                  accept="image/png,image/jpeg"
                  type="file"
                  
                  onChange={handleImageChange}
                />
                <SimpleGrid
                  spacing={10}
                  columns={{ xl: 3, lg: 3, md: 3, sm: 2 }}
                >
                  {imagePreviews.map((preview, index) => (
                    <GridItem
                      key={index}
                      backgroundImage={preview}
                      alt={`Preview ${index}`}
                      bgSize="cover"
                      bgPosition="center"
                      bgRepeat="no-repeat"
                      width="200px"
                      height="200px"
                    >
                      <Flex justify="end">
                        <RiChatDeleteFill 
                          color="teal"
                          size={20}
                          onClick={() => handleRemoveImage(index)}
                        />
                      </Flex>
                    </GridItem>
                  ))}
                </SimpleGrid>
              </VStack>
            </FormControl>
          </ModalBody>

          <ModalFooter>
            <Button
              mr={3}
              colorScheme="green"
              type="submit"
              onClick={handleSubmitForm}
            >
              Submit
            </Button>
            <Button onClick={onClose}>Cancel</Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </>
  );
}

export default PostForm;
