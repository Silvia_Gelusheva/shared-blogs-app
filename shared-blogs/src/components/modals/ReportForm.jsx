import {
  Button,
  Checkbox,
  CheckboxGroup,
  FormControl,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  Stack,
  Textarea,
  useDisclosure,
} from "@chakra-ui/react";
import { useDispatch, useSelector } from "react-redux";
import { useRef, useState } from "react";

import { reportPost } from "../../redux/actions/post-actions";

const ReportForm = ({ post, isInDropdown }) => {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const dispatch = useDispatch();
  const authUser = useSelector((state) => state.user.user);

  const [selectedArguments, setSelectedArguments] = useState([]);
  const [message, setMessage] = useState("");
  const initialRef = useRef(null);
  const finalRef = useRef(null);

  const handleCheckboxChange = (value) => {
    if (selectedArguments.includes(value)) {
      setSelectedArguments(
        selectedArguments.filter((option) => option !== value)
      );
    } else {
      setSelectedArguments([...selectedArguments, value]);
    }
  };
 
  const handleReportPost = () => {
    try {
       dispatch(
        reportPost([
          post,
          authUser.displayName,
          selectedArguments,
          message,
        ])
      );
      onClose();
    } catch (error) {
      console.log("Report failed:", error.message);
    }
  };

  return (
    <>
        <Button size="sm" onClick={onOpen}> {isInDropdown ? 'Report post': '👮 Report' }</Button>

      <Modal
        initialFocusRef={initialRef}
        finalFocusRef={finalRef}
        isOpen={isOpen}
        onClose={onClose}
      >
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>Report post</ModalHeader>
          <ModalCloseButton />
          <ModalBody pb={6}>
            <CheckboxGroup colorScheme="green" defaultValue={["inappropriate"]}>
              <Stack spacing={[1, 5]} direction={["column", "column"]}>
                {["Inappropriate content", "Offensive content", "Other"].map(
                  (option) => (
                    <Checkbox
                      key={option}
                      isChecked={selectedArguments.includes(option)}
                      onChange={() => handleCheckboxChange(option)}
                    >
                      {option}
                    </Checkbox>
                  )
                )}
              </Stack>
            </CheckboxGroup>
            <FormControl mt={4}>
              <Textarea
                placeholder="Type here"
                value={message}
                onChange={(e) => setMessage(e.target.value)}
              />
            </FormControl>
          </ModalBody>

          <ModalFooter>
            <Button colorScheme="blue" mr={3} onClick={handleReportPost}>
              Submit
            </Button>
            <Button onClick={onClose}>Cancel</Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </>
  );
};

export default ReportForm;
