import {
  Box,
  Button,
  Container,
  Heading,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalOverlay,
  Stack,
  Text,
  useDisclosure,
} from "@chakra-ui/react";

import { Carousel } from "react-responsive-carousel";
import { useRef } from "react";
import { useState } from "react";

const QuickView = ({ post }) => {
  const OverlayOne = () => (
    <ModalOverlay
      bg="blackAlpha.300"
      backdropFilter="blur(10px) hue-rotate(90deg)"
    />
  );
  const [overlay, setOverlay] = useState(<OverlayOne />);
  const initialRef = useRef(null);
  const finalRef = useRef(null);
  const { isOpen, onOpen, onClose } = useDisclosure();
  return (
    <>
      <Button
        variant="ghost"
        onClick={() => {
          setOverlay(<OverlayOne />);
          onOpen();
        }}
        w="full"
      >
        Quick review
      </Button>

      <Modal
        initialFocusRef={initialRef}
        finalFocusRef={finalRef}
        onClose={onClose}
        size="5xl"
        isOpen={isOpen}
      >
        {overlay}
        <ModalOverlay />
        <ModalContent>
          <ModalCloseButton />
          <ModalBody pb={6}>
            <Container maxW={"5xl"}>
              <Stack
                textAlign={"center"}
                align={"center"}
                spacing={{ base: 2, md: 2 }}
                py={{ base: 5, md: 4 }}
                fontSize={25}
              >
                <Heading
                  fontWeight={600}
                  fontSize={{ base: "xl", sm: "2xl", md: "3xl" }}
                  lineHeight={"110%"}
                >
                  {post.title}
                </Heading>

                <Text
                  fontStyle="italic"
                  fontSize={{ base: "md", sm: "md", md: "md" }}
                >
                  by {post.author}
                </Text>
                <Box align="center">
                  <Carousel infiniteLoop dynamicHeight width="400px">
                    {post?.urls?.map((url) => {
                      return (
                        <img
                          key={post.postId}
                          src={url}
                          alt={post?.title}
                          height="300px"
                          width="300px"
                        />
                      );
                    })}
                  </Carousel>
                </Box>
                <Text
                  dangerouslySetInnerHTML={{ __html: post.text }}
                  className="mt-3 text-base"
                />
              </Stack>
            </Container>
          </ModalBody>
        </ModalContent>
      </Modal>
    </>
  );
};

export default QuickView;
