import { useDispatch, useSelector } from "react-redux";

import Request from "./Request";
import UserMessages from "./UserMessages";
import { fetchAllRequests } from "../../../redux/actions/requests-actions";
import { useDisclosure } from "@chakra-ui/react";
import { useEffect } from "react";

const RequestForm = ({ currentUser }) => {
  const dispatch = useDispatch();
  const { isOpen, onOpen, onClose } = useDisclosure();
  const requests = useSelector((state) => state.requests.requests);
  useEffect(() => {
    dispatch(fetchAllRequests());
  }, [dispatch]);
    
    const isRequested =
    requests &&
    Object.values(requests).some(
      (request) => request?.displayName === currentUser?.displayName
    );


    const request =
    requests &&
    Object.values(requests).filter(
      (request) => request?.displayName === currentUser?.displayName
    )

    console.log("req",request);
  return (
    <>
      {!isRequested ? (
        <Request
          currentUser={currentUser}      
          isRequested={isRequested}
        />
      ) : (
        <UserMessages       
          isRequested={isRequested}
          request={request[0]}
        />
      )}
  
    </>
  );
};

export default RequestForm;
