import {
  Button,
  Checkbox,
  Flex,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  Text,
  Textarea,
  useDisclosure,
} from "@chakra-ui/react";
import {
  fetchAllRequests,
  makeRequest,
} from "../../../redux/actions/requests-actions";

import { useDispatch } from "react-redux";
import { useState } from "react";

const Request = ({ currentUser, isRequested }) => {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const dispatch = useDispatch();
  const [requestText, setRequestText] = useState("");
  const [confirmDelete, setConfirmDelete] = useState(false);
  const [confirmAdmin, setConfirmAdmin] = useState(false);
  const [confirmPromoteProduct, setConfirmPromoteProduct] = useState(false);
  const [confirmPromoteEvent, setConfirmPromoteEvent] = useState(false);

  const requestData = {
    displayName: currentUser?.displayName,
    confirmPromoteProduct: confirmPromoteProduct,
    confirmPromoteEvent: confirmPromoteEvent,
    confirmAdmin: confirmAdmin,
    text: requestText,
    confirmDelete: confirmDelete,
    response: null,
  };

  const handleRequestSubmit = () => {
    if (
      (confirmDelete ||
        confirmAdmin ||
        confirmPromoteProduct ||
        confirmPromoteEvent) &&
      requestText
    ) {
      dispatch(
        makeRequest({ request: requestData, displayName: currentUser?.displayName })
      );
      setRequestText("");
      setConfirmDelete(false);
      dispatch(fetchAllRequests());
      onClose();
    } else {
      alert("Please, add message!");
    }
  };

  return (
    <>
      <Button size="sm" colorScheme="gray" onClick={!isRequested && onOpen}>
        ✉ Request
      </Button>
      <Modal isOpen={isOpen} onClose={onClose}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader></ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            <Flex direction="column">
              <Checkbox
                textColor="gray"
                colorScheme="green"
                size="lg"
                mb={4}
                required
                isChecked={confirmPromoteProduct}
                onChange={() =>
                  setConfirmPromoteProduct(!confirmPromoteProduct)
                }
              >
                <Text fontSize="md">I want to promote product/s</Text>
              </Checkbox>
              <Checkbox
                textColor="gray"
                colorScheme="green"
                size="lg"
                mb={4}
                required
                isChecked={confirmPromoteEvent}
                onChange={() => setConfirmPromoteEvent(!confirmPromoteEvent)}
              >
                <Text fontSize="md">I want to promote event/s</Text>
              </Checkbox>
              <Checkbox
                textColor="gray"
                colorScheme="green"
                size="lg"
                mb={4}
                required
                isChecked={confirmAdmin}
                onChange={() => setConfirmAdmin(!confirmAdmin)}
              >
                <Text fontSize="md">I want to be an Admin</Text>
              </Checkbox>
              <Checkbox
                textColor="gray"
                colorScheme="green"
                size="lg"
                mb={4}
                required
                isChecked={confirmDelete}
                onChange={() => setConfirmDelete(!confirmDelete)}
              >
                <Text fontSize="md">I want my profile deleted</Text>
              </Checkbox>
              <Textarea
                required
                placeholder="Additional information..."
                value={requestText}
                onChange={(e) => setRequestText(e.target.value)}
              />
            </Flex>
          </ModalBody>

          <ModalFooter>
            <Button colorScheme="teal" mr={3} onClick={handleRequestSubmit}>
              Send Request
            </Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </>
  );
};

export default Request;
