import {
  Avatar,
  Badge,
  Button,
  Flex,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  Text,
  Textarea,
  useDisclosure,
} from "@chakra-ui/react";
import {
  setIsNewAdminMessageFalse,
  userSendMessage,
} from "../../../redux/actions/requests-actions";
import { useEffect, useState } from "react";

import { useDispatch } from "react-redux";

const UserMessages = ({ request, isRequested }) => {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const dispatch = useDispatch();
  const [userMessageText, setUserMessageText] = useState("");
  const [allMessages, setAllMessages] = useState([]);

  useEffect(() => {
    const mergedMessages = [
      ...(request?.adminMessages || []),
      ...(request?.userMessages || []),
    ];
    const sortedMessages = mergedMessages.sort(
      (a, b) => a.timestamp - b.timestamp
    );
    setAllMessages(sortedMessages);
  }, [request?.adminMessages, request?.userMessages]);

  const handleSendMessage = () => {
    if (userMessageText && request?.displayName) {
      dispatch(
        userSendMessage({
          message: userMessageText,
          displayName: request?.displayName,
        })
      )
        .then(() => {
          setUserMessageText("");  
        })
        .catch((error) => {
          console.error("Error user sending message:", error);
        });
    } else {
      alert("Please, add message!");
    }
  };

  const handleOpenButton = () => {
    dispatch(setIsNewAdminMessageFalse( request?.displayName ));
    onOpen();
  };

  return (
    <>
      <Button
        size="sm"
        colorScheme="gray"
        onClick={isRequested && handleOpenButton}
      >
        {!request?.adminMessages ? "⏳ Pending" : "Response"}
        {request?.isNewAdminMessage && (
          <Badge  ml="2">📩</Badge>
          )}
      </Button>

      <Modal
        blockScrollOnMount={false}
        isOpen={isOpen}
        onClose={onClose}
        size="xl"
      >
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>Admin     
          </ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            <Flex direction="column" w="full" h="400px" overflowY="auto" p={4}>
              {allMessages.map((m, index) => (
                <Flex key={index} align="center" gap="2" my={1}>
                  {!m.hasOwnProperty("displayName") && (
                    <Avatar size="xs" name="Admin" />
                  )}
                  <Flex
                    ml={m.hasOwnProperty("displayName") ? "auto" : "0"}
                    bg={m.hasOwnProperty("displayName") ? "blue.600" : "gray.600"}
                    color={m.hasOwnProperty("displayName") ? "white" : "white"}
                    p={2}
                    borderRadius="md"
                    marginBottom="2"
                    maxW="fit-content"
                  >
                    <Text fontSize="sm" maxWidth="300px">{m.message}</Text>
                  </Flex>
                </Flex>
              ))}
            </Flex>

            <Textarea
              mt={2}
              placeholder="Send message..."
              value={userMessageText}
              onChange={(e) => setUserMessageText(e.target.value)}
            />
          </ModalBody>

          <ModalFooter>           
            <Button colorScheme="gray" mr={3} onClick={handleSendMessage}>
              Submit
            </Button>
      
          </ModalFooter>
        </ModalContent>
      </Modal>
    </>
  );
};

export default UserMessages;
