import {
  Avatar,
  Badge,
  Button,
  Divider,
  Flex,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  Text,
  Textarea,
  useDisclosure,
} from "@chakra-ui/react";
import {
  adminSendMessage,
  setIsNewUserMessageFalse
} from "../../../redux/actions/requests-actions";
import { useEffect, useState } from "react";

import { useDispatch } from "react-redux";

const AdminMessages = ({ request }) => {
  const dispatch = useDispatch();
  const { isOpen, onOpen, onClose } = useDisclosure();
  const [adminMessageText, setAdminMessageText] = useState("");
  const [allMessages, setAllMessages] = useState([]);

  useEffect(() => {
    const mergedMessages = [
      ...(request?.userMessages || []),
      ...(request?.adminMessages || []),
    ];
    const sortedMessages = mergedMessages.sort(
      (a, b) => a.timestamp - b.timestamp
    );
    setAllMessages(sortedMessages);
  }, [request?.userMessages, request?.adminMessages]);

  const handleSendMessage = () => {
    if (adminMessageText && request?.displayName) {
      dispatch(
        adminSendMessage({
          message: adminMessageText,
          displayName: request?.displayName,       
        })
      )
        .then(() => {
          setAdminMessageText("");      
       })
        .catch((error) => {
          console.error("Error sending message:", error);
        });
    } else {
      alert("Please, add message!");
    }
  };
  const handleOpenButton = () => {
    dispatch(setIsNewUserMessageFalse( request?.displayName ));
    onOpen()
};
  return (
    <>
      <Button colorScheme="blue" size="sm" onClick={handleOpenButton}>
        Messages
        {request?.isNewUserMessage && (
          <Badge size="sm" colorScheme="red" style={{ marginLeft: "5px", fontWeight: "bold" }}>New</Badge>
        )}
      </Button>
      <Modal
        blockScrollOnMount={false}
        isOpen={isOpen}
        onClose={onClose}
        size="xl"
      >
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>{request?.displayName}</ModalHeader>
          <Divider />
          <ModalCloseButton />
          <ModalBody>
            <Flex direction="column" w="full" h="400px" overflowY="auto" p={4}>
              {allMessages.map((m, index) => (
                <Flex key={index} align="center" gap={3} my={1}>
                  {m.hasOwnProperty("displayName") && (
                    <Avatar size="xs" name={m.displayName} />
                  )}
                  <Flex
                    ml={m.hasOwnProperty("displayName") ? "0" : "auto"}
                    bg={
                      m.hasOwnProperty("displayName") ? "gray.600" : "blue.600"
                    }
                    color={m.hasOwnProperty("displayName") ? "white" : "white"}
                    p={2}
                    borderRadius="md"
                    marginBottom="2"
                    maxW="fit-content"
                  >
                    <Text fontSize="sm" maxWidth="300px">{m.message}</Text>
                  </Flex>
                </Flex>
              ))}
            </Flex>

            <Textarea
              required
              placeholder="Message text..."
              value={adminMessageText}
              onChange={(e) => setAdminMessageText(e.target.value)}
            />
          </ModalBody>
          <ModalFooter>
            <Button size="md" colorScheme="gray" mr={3} onClick={handleSendMessage}>
              Submit
            </Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </>
  );
};

export default AdminMessages;
