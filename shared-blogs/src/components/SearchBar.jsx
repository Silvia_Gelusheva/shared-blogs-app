import { Flex, Input, InputGroup, InputLeftElement } from "@chakra-ui/react";

import { FaSearch } from "react-icons/fa";

const SearchBar = ({ handleChange, inputRef }) => {
  return (
    <Flex>
      <InputGroup>
        <InputLeftElement
          pointerEvents="none"
          children={<FaSearch color="gray.300" />}
        />
        <Input
          name=""
          type="text"
          placeholder="Search..."
          rounded="full"
          ref={inputRef}
          onChange={handleChange}
        />
      </InputGroup>
    </Flex>
  );
};

export default SearchBar;
