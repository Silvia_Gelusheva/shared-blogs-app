import { Button, Flex } from "@chakra-ui/react";
import { useEffect, useState } from "react";

import AboutUser from "./AboutUser";
import BlockUser from "./BlockUser";
import RequestForm from "../modals/requests/RequestForm";
import { getUserByName } from "../../redux/actions/user-actions";
import { useNavigate } from "react-router-dom";

const ProfileButtons = ({ currentUser, authUser }) => {
  const [isAdmin, setIsAdmin] = useState(false);
  const navigate = useNavigate();
  useEffect(() => {
    getUserByName(authUser?.displayName).then((res) =>
      setIsAdmin(res.role === 2)
    );
  }, []);

  return (
    <Flex
      direction="row"
      justify={{ base: "center", md: "center", xl: "space-between" }}
      align="center"
      gap={4}
    >
      {authUser?.displayName !== currentUser?.displayName && (
        <AboutUser currentUser={currentUser} />
      )}
      {authUser?.displayName === currentUser?.displayName && (
        <Button
          size="sm"
          colorScheme="gray"
          onClick={() =>
            navigate(`/users/${currentUser?.displayName}/edit-user`)
          }
        >
          🛠️ Edit
        </Button>
      )}

      {currentUser?.displayName !== authUser?.displayName && (
        <Button size="sm" colorScheme="gray">
          📭 Message
        </Button>
      )}
      {authUser?.displayName === currentUser?.displayName && (
        <RequestForm currentUser={currentUser} />
      )}

      {isAdmin && currentUser?.displayName !== authUser?.displayName && (
        <BlockUser currentUser={currentUser} />
      )}
    </Flex>
  );
};

export default ProfileButtons;
