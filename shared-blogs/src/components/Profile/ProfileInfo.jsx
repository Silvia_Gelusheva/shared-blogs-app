import { Avatar, Center, Text, VStack } from "@chakra-ui/react";

const ProfileInfo = ({ currentUser }) => {
  return (
    <Center gap={4} >
    <Avatar size="2xl" name={currentUser?.name} src={currentUser?.avatar} />
    <VStack align="start">
      <Text>
        <strong>User:</strong> {currentUser?.displayName}
      </Text>
      <Text>
        <strong>Joined on:</strong>  {`${new Date(currentUser?.joinedOn).toLocaleDateString("en-GB")}`}
      </Text>
      <Text>
        <strong>Email:</strong> {currentUser?.email}
      </Text>
    </VStack>
  </Center>
  );
};

export default ProfileInfo;
