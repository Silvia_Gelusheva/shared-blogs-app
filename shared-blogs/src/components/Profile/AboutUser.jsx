import {
  Box,
  Button,
  Flex,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  Text,
  useDisclosure,
} from "@chakra-ui/react";
import { IoHome, IoPerson } from "react-icons/io5";

import { IoMdHeart } from "react-icons/io";
import { MdOutlineWork } from "react-icons/md";

const AboutUser = ({ currentUser }) => {
  const { isOpen, onOpen, onClose } = useDisclosure();
  return (
    <>
      <Button size="sm" colorScheme="gray" onClick={onOpen}>
        🕵🏻 Details
      </Button>
      <Modal isOpen={isOpen} onClose={onClose}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>{currentUser?.displayName}</ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            <Flex
              direction="column"
              align="flex-start"
              justify="center"
              p={6}
              borderRadius="lg"
              boxShadow="md"
              maxW="sm"
              w="full"
              borderWidth="1px"
              borderColor="gray.200"
            >
              <Text fontSize="sm" color="gray.500" mb={4}>
                {currentUser?.occupation}
              </Text>
              <Box>
                <Flex direction="row" align="center" mb={2}>
                  <IoPerson fontSize="1.2rem" color="gray.600" />
                  <Text fontSize="sm" ml={2}>
                    {currentUser?.name} {currentUser?.lastName}
                  </Text>
                </Flex>
                <Flex direction="row" align="center" mb={2}>
                  <IoHome fontSize="1.2rem" color="gray.600" />
                  <Text fontSize="sm" ml={2}>
                    {currentUser?.livesIn}
                  </Text>
                </Flex>
                <Flex direction="row" align="center" mb={2}>
                  <MdOutlineWork fontSize="1.2rem" color="gray.600" />
                  <Text fontSize="sm" ml={2}>
                    {currentUser?.occupation}
                  </Text>
                </Flex>
                <Flex direction="row" align="center">
                  <IoMdHeart fontSize="1.2rem" color="gray.600" />
                  <Text fontSize="sm" ml={2}>
                    {currentUser?.interestedIn}
                  </Text>
                </Flex>
              </Box>
            </Flex>
          </ModalBody>

          <ModalFooter>
            <Button colorScheme="blue" mr={3} onClick={onClose}>
              Close
            </Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </>
  );
};

export default AboutUser;
