import {
  Box,
  Button,
  Flex,
  FormControl,
  FormLabel,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  Text,
  useDisclosure,
} from "@chakra-ui/react";
import {
  fetchAllUsers,
  fetchUserProfile,
  updateUserAsyncThunk,
} from "../../redux/actions/user-actions";

import { useDispatch } from "react-redux";
import { useState } from "react";

const BlockUser = ({ currentUser }) => {
  const dispatch = useDispatch();
  const { isOpen, onOpen, onClose } = useDisclosure();
  const [newUserData, setNewUserData] = useState({
    isBlocked: currentUser?.isBlocked || false,
  });
  let isBlocked = currentUser?.isBlocked;
  const handleSaveChanges = () => {
    dispatch(updateUserAsyncThunk(newUserData));
    dispatch(fetchUserProfile(currentUser?.displayName));
    onClose();
  };
  
  const handleToggleBlock = () => {
    setNewUserData({ ...newUserData, isBlocked: !newUserData.isBlocked });
};


  return (
    <>
      <Button size="sm" colorScheme="gray" onClick={onOpen}>
        {!currentUser?.isBlocked ? "⛔ Block" : "✅ Unblock"}
      </Button>
      <Modal isOpen={isOpen} onClose={onClose}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>
            {isBlocked
              ? `${currentUser?.displayName} is currently blocked!`
              : `${currentUser?.displayName}  is currently active!`}
          </ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            <Flex
              direction="column"
              align="flex-start"
              justify="center"
              p={6}
              borderRadius="lg"
              boxShadow="md"
              maxW="sm"
              w="full"
              borderWidth="1px"
              borderColor="gray.200"
            >
              <FormControl mt={4} align="center">
                <Button
                  colorScheme={newUserData.isBlocked ? "green" : "red"}
                  onClick={handleToggleBlock}
                >
                  {newUserData.isBlocked ? "Unblock user" : "Block user"}
                </Button>
              </FormControl>
            </Flex>
          </ModalBody>

          <ModalFooter>
            <Button colorScheme="teal" mr={3} onClick={handleSaveChanges}>
              Save
            </Button>
            <Button colorScheme="gray" onClick={onClose}>
              Close
            </Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </>
  );
};

export default BlockUser;
