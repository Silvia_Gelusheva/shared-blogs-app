import { Button, HStack } from "@chakra-ui/react";

const Pagination = ({ currentPage, totalPages, onPageChange }) => {
  const pageButtons = Array.from(
    { length: totalPages },
    (_, index) => index + 1
  );

  return (
    <HStack spacing={2} mb={4}>
      {pageButtons.map((page) => (
        <Button
          size="sm"
          key={page}
          onClick={() => onPageChange(page)}
          colorScheme={currentPage === page ? "teal" : "gray"}
        >
          {page}
        </Button>
      ))}
    </HStack>
  );
};

export default Pagination;
