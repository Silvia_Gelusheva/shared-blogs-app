import { Avatar, Box, Flex, Text } from "@chakra-ui/react";

import { Link } from "react-router-dom";

const AdminCard = ({ user }) => {
  return (
    <Box
      maxW="md"
      borderWidth="1px"
      borderRadius="lg"
      overflow="hidden"
      p={4}
      boxShadow="md"
    >
      <Link to={`${user.displayName}`}>
        <Flex alignItems="center" cursor="pointer" mb={4}>
          <Avatar src={user.avatar} name={user.displayName} size="lg" />
          <Box ml={4}>
            <Text fontSize="xl" fontWeight="bold">
              {user.displayName}
            </Text>
            <Text color="gray.500">
              {user.name} {user.lastName}
            </Text>
          </Box>
        </Flex>
      </Link>
      <Flex direction="normal" gap={2} fontSize="sm">
        <Text fontWeight="normal">Member since:</Text>
        <Text>{`${new Date(user?.joinedOn).toLocaleDateString("en-GB")}`}</Text>
      </Flex>
      <Flex direction="row" gap={2} mt={2}>
        <Text fontWeight="normal">e-mail:</Text>
        <Text fontWeight="normal" fontStyle="italic" color="blue.500">
          {" "}
          {user.email}
        </Text>
      </Flex>
    </Box>
  );
};

export default AdminCard;
