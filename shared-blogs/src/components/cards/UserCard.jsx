import { Avatar, Box, Flex, Text } from "@chakra-ui/react";

import { Link } from "react-router-dom";

const UserCard = ({ user }) => {
  return (
    <Link to={`${user.displayName}`}>
      <Box
        maxW="md"
        borderWidth="1px"
        borderRadius="lg"
        overflow="hidden"
        p={4}
        boxShadow="md"
        cursor="pointer"
      >
        <Flex alignItems="center" mb={4}>
          <Avatar src={user.avatar} name={user.name} size="lg" />
          <Box ml={4}>
            <Text fontSize="xl" fontWeight="bold">
              {user.displayName}
            </Text>
            <Text color="gray.500">
              {user.name} {user.lastName}
            </Text>
          </Box>
        </Flex>
        <Flex direction="row" gap={2} fontSize="sm">
          <Text fontWeight="bold">Member since:</Text>
          <Text>{`${new Date(user?.joinedOn).toLocaleDateString("en-GB")}`}</Text>
        </Flex>
        <Flex direction="row" gap={2} mt={2}>
          <Text fontWeight="bold">Blogs:</Text>
          <Text fontWeight="bold">{(user.posts && Object.keys(user.posts).length) || 0}</Text>
        </Flex>
      </Box>
    </Link>
  );
};

export default UserCard;
