import {
  Avatar,
  Box,
  Flex,
  IconButton,
  Image,
  Spacer,
  Text,
} from "@chakra-ui/react";
import { useEffect, useState } from "react";

import DropdownMenu from "../../pages/DropdownMenu";
import { FaRegHeart } from "react-icons/fa";
import { FaRegStar } from "react-icons/fa6";
import { getUserByName } from "../../redux/actions/user-actions";
import { useNavigate } from "react-router-dom";

const TrendingBlogCard = ({ post }) => {
   const navigate = useNavigate(); 

   const [currentAuthor, setCurrentAuthor] = useState({});
  
   useEffect(() => {
     getUserByName(post.author).then(setCurrentAuthor);
   }, [post]);
 
  
  return (
    <Box p={4} borderWidth="1px" borderRadius="lg" overflow="hidden">
      <Flex align="center" justify="start" gap={2}>
        <Avatar src={currentAuthor?.avatar} name={currentAuthor?.avatar} size="sm" mb={2} />
        <Text fontSize="sm" color="gray.500" mb={2}>
          {post?.author}
        </Text>
      </Flex>
      <Image
        src={post?.urls ? post?.urls[0] : post?.url}
        alt="Blog Image"
        w="full"
        mb={4}
        borderRadius="lg"
        onClick={() => navigate(`/${post?.postId}`)}
        cursor="pointer"
      
      />
      <Text fontWeight="bold"> {post?.title}</Text> 
      <Flex mt={4} align="center">
        <IconButton icon={<FaRegHeart />} size="sm" colorScheme="red" />
        <Text ml={1} mr={2}>
          {" "}
          {post.likedBy && Object.keys(post.likedBy).length}{" "}
        </Text>
        <IconButton icon={<FaRegStar />} size="sm" colorScheme="yellow" />
        <Text ml={1}> {post.savedBy && Object.keys(post.savedBy).length}{" "} </Text>
        <Spacer />
        <DropdownMenu post={post} />
      </Flex>
    </Box>
  );
};

export default TrendingBlogCard;
