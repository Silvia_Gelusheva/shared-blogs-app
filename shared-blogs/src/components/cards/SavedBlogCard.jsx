import {
  Avatar,
  Box,
  Button,
  Card,
  CardBody,
  CardFooter,
  Flex,
  Heading,
  Image,
  Stack,
  Text,
} from "@chakra-ui/react";
import { useEffect, useState } from "react";

import DropdownMenu from "../../pages/DropdownMenu";
import { getUserByName } from "../../redux/actions/user-actions";
import { useNavigate } from "react-router-dom";

const SavedBlogCard = ({ post }) => {
  const [currentAuthor, setCurrentAuthor] = useState({});
  const navigate = useNavigate();

  useEffect(() => {
    getUserByName(post.author).then(setCurrentAuthor);
  }, [post]);

  return (
    <Card
      direction={{ base: "column", lg: "row" }}
      overflow="hidden"
      variant="outline"
      padding={4}
    >
      <Image
        objectFit="cover"
        w={{ base: "100%", sm: "200px" }}
        h="200px"
        src={post?.urls ? post?.urls[0] : post?.url || "/public/noImage.jpg"}
        alt="cover"
      />

      <Stack w="full">
        <CardBody w="full">
          <Heading size="md" mb={4}>
            {post.title}
          </Heading>
          <Text
            fontSize="sm"
            dangerouslySetInnerHTML={{
              __html: post?.text?.substring(0, 250),
            }}
          />
        </CardBody>

        <CardFooter w="full">
          <Flex w="full" justify="space-between" flexWrap="wrap" gap={2}>
            <Flex align="center" flexWrap="wrap" gap={3}>
              <Avatar size="sm" name={currentAuthor?.displayName} src={currentAuthor?.avatar} />
              <Box>
                <Text fontSize="xs">
                  {currentAuthor?.name} {currentAuthor?.lastname}
                </Text>
                <Text fontSize="xs">
                  {`${new Date(post?.addedOn).toLocaleDateString("en-GB")}`}
                </Text>
              </Box>
            </Flex>

            <Flex align="center" gap={3}>
              <Button
                variant="solid"
                onClick={() => navigate(`/${post?.postId}`)}
              >
                Read Blog
              </Button>
              <DropdownMenu post={post} />
            </Flex>
          </Flex>
        </CardFooter>
      </Stack>
    </Card>
  );
};

export default SavedBlogCard;
