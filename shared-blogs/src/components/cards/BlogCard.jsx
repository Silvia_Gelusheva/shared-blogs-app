import { Box, Button, Flex, Heading, Image, Text } from "@chakra-ui/react";

import DropdownMenu from "../../pages/DropdownMenu";
import { useNavigate } from "react-router-dom";

const BlogCard = ({ post }) => {
  const navigate = useNavigate();

  return (
    <Box p={4} borderWidth="1px" borderRadius="lg">
      <Image
        src={post?.urls ? post?.urls[0] : post?.url}
        alt="No image available"
        mb={4}
        w="full"
        borderRadius="lg"
      />
      <Heading as="h3" size="md" mb={2}>
        {post.title}
      </Heading>
      <Text fontSize="sm" color="gray.500" mb={2}>
        by {post.author} on{" "}
        {`${new Date(post?.addedOn).toLocaleDateString("en-GB")}`}
      </Text>
      <Text
        fontSize="sm"
        dangerouslySetInnerHTML={{
          __html: post?.text?.substring(0, 250),
        }}
      />

      <Flex gap={2} mt={3}>
        <Button colorScheme="gray" onClick={() => navigate(`/${post?.postId}`)}>
          Read Blog
        </Button>
        <DropdownMenu post={post} />
      </Flex>
    </Box>
  );
};

export default BlogCard;
