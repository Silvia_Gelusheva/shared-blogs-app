import "./index.css";

import { ChakraProvider, ColorModeScript } from "@chakra-ui/react";
import store, { persistor } from "./redux/store";

import App from "./App.jsx";
import { BrowserRouter } from "react-router-dom";
import { PersistGate } from 'redux-persist/integration/react';
import { Provider } from "react-redux";
import React from "react";
import ReactDOM from "react-dom/client";

ReactDOM.createRoot(document.getElementById("root")).render(
  <React.StrictMode>
    <ChakraProvider>
      <BrowserRouter>
        <Provider store={store}>
        {/* <PersistGate loading={null} store={store}> */}
          <App />
          {/* </PersistGate> */}
        </Provider>
      </BrowserRouter>
    </ChakraProvider>
  </React.StrictMode>
);
