import { deleteSavedPostByUser, fetchAllSavedPostsByUser, removeFromSaved, savePost } from '../actions/saved-actions';

import { createSlice } from '@reduxjs/toolkit';

const SavedSlice = createSlice({
    name: 'saved',
    initialState: {
        ids: [],
        status: 'idle',
        error: null,
    },
    reducers: {
        setIds: (state, action) => {
            return action.payload;
        },
        setLoading: (state, action) => {
            state.status = "loading";
        },
        setError: (state, action) => {
            state.status = "failed";
            state.error = action.payload;
        },

    },
    extraReducers: (builder) => {
        builder
            .addCase(fetchAllSavedPostsByUser.pending, (state) => {
                state.status = 'loading';
            })
            .addCase(fetchAllSavedPostsByUser.fulfilled, (state, action) => {
                state.status = 'succeeded';
                state.ids = action.payload;

            })
            .addCase(fetchAllSavedPostsByUser.rejected, (state, action) => {
                state.status = 'failed';
                state.error = action.error.message;
            })
            .addCase(savePost.fulfilled, (state, action) => {
                state.ids.push(action.payload);


            })
            .addCase(removeFromSaved.fulfilled, (state, action) => {
                state.ids = state.ids.filter((id) => id !== action.payload)
            })
            .addCase(deleteSavedPostByUser.fulfilled, (state, action) => {
                state.ids = []
            })
    },
});
export const { setIds, setLoading, setError } = SavedSlice.actions;
export default SavedSlice.reducer;


