import { createSlice } from '@reduxjs/toolkit';

const ProfileSlice = createSlice({
  name: 'profile',
  initialState: {
    user: null,
  },
  reducers: {
    setUserProfile: (state, action) => {
      state.user = action.payload;
    },
  },
});

export const { actions: userProfileActions } = ProfileSlice;
export default ProfileSlice.reducer;
