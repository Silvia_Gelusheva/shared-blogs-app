import { addPostToFirebase, deletePostAsyncThunk, dismissAllPostReports, dismissReport, fetchAllPosts, fetchAllReports, reportPost, toggleBlockPost, toggleLikePost, updatePostAsyncThunk } from '../actions/post-actions';
import { createSelector, createSlice } from '@reduxjs/toolkit';

const postsSlice = createSlice({
    name: 'posts',
    initialState: {
        posts: [],
        postsContainer: [],
        reports: {},
        status: 'idle',
        error: null,
    },
    reducers: {
        setPosts: (state, action) => {
            return action.payload;
        },
        filteredPosts: (state, action) => {
            state.posts = state.postsContainer.filter((post) =>
                post.title.toLowerCase().includes(action.payload) || post.text.toLowerCase().includes(action.payload) || post.author.toLowerCase().includes(action.payload)
            )
        },
    },
    extraReducers: (builder) => {
        builder
            .addCase(fetchAllPosts.pending, (state) => {
                state.status = 'loading';
            })
            .addCase(fetchAllPosts.fulfilled, (state, action) => {
                state.status = 'succeeded';
                state.posts = action.payload;
                state.postsContainer = action.payload;
            })
            .addCase(fetchAllPosts.rejected, (state, action) => {
                state.status = 'failed';
                state.error = action.error.message;
            })
            .addCase(addPostToFirebase.fulfilled, (state, action) => {
                state.posts.push(action.payload);
                state.postsContainer.push(action.payload);

            })
            .addCase(updatePostAsyncThunk.fulfilled, (state, action) => {
                const { postId, updatedData } = action.payload;
                const postIndex = state.posts.findIndex(post => post.postId === postId);

                if (postIndex !== -1) {
                    state[postIndex] = { ...state[postIndex], ...updatedData };
                }
            })
            .addCase(deletePostAsyncThunk.fulfilled, (state, action) => {
                state.posts = state.posts.filter((post) => post.postId !== action.payload[0])
                state.postsContainer = state.postsContainer.filter((post) => post.postId !== action.payload[0])
            })
            .addCase(toggleBlockPost.fulfilled, (state, action) => {
                const toggledPost = action.payload;
                const postIndex = state.posts.findIndex((post) => post.postId === toggledPost.postId);

                if (postIndex !== -1) {
                    state.posts[postIndex].isBlocked = !toggledPost.isBlocked;
                }
            })

            .addCase(reportPost.fulfilled, (state, action) => {
                const { reportId, postId, reportedBy, selectedArguments, message } = action.payload;
                const postIndex = state.posts.findIndex((post) => post.postId === postId);

                if (postIndex !== -1) {
                    if (!state.posts[postIndex].reports) {
                        state.posts[postIndex].reports = {};
                    }

                    state.posts[postIndex].reports[reportId] = {
                        reportId, postId, reportedBy, selectedArguments, message,
                    };

                    state.reports = {
                        ...state.reports,
                        [reportId]: {
                            reportId, postId, reportedBy, selectedArguments, message,
                        },
                    };

                }
            })
            .addCase(dismissReport.fulfilled, (state, action) => {
                const [postId, displayName] = action.payload;
                const postIndex = state.posts.findIndex((post) => post.postId === postId);

                if (postIndex !== -1) {
                    if (state.posts[postIndex].reports) {
                        state.posts[postIndex].reports = Object.fromEntries(
                            Object.entries(state.posts[postIndex].reports).filter(
                                ([reportId, report]) => report.reportedBy !== displayName
                            )
                        );
                    }
                }

                state.reports = Object.fromEntries(
                    Object.entries(state.reports).filter(
                        ([reportId, report]) => !(report.postId === postId && report.reportedBy === displayName)
                    )
                );
            })
            .addCase(dismissAllPostReports.fulfilled, (state, action) => {
                const postId = action.payload;
                const postIndex = state.posts.findIndex((post) => post.postId === postId);
                if (postIndex !== -1 && state.posts[postIndex].reports) {
                    state.posts[postIndex].reports = {};
                }

                state.reports = Object.fromEntries(
                    Object.entries(state.reports).filter(
                        ([reportId, report]) => report.postId !== postId
                    )
                );
            })
            .addCase(fetchAllReports.fulfilled, (state, action) => {
                state.status = 'succeeded';
                state.reports = action.payload;
            })
            .addCase(toggleLikePost.fulfilled, (state, action) => {
                const [postId, displayName] = action.payload;
                const postIndex = state.posts.findIndex((post) => post.postId === postId);

                if (postIndex !== -1) {
                    const isLiked = state.posts[postIndex].likedBy && state.posts[postIndex].likedBy[displayName];

                    if (isLiked) {
                        state.posts[postIndex].likedBy = {
                            ...state.posts[postIndex].likedBy,
                            [displayName]: null,
                        };
                    } else {
                        state.posts[postIndex].likedBy = {
                            ...state.posts[postIndex].likedBy,
                            [displayName]: true,
                        };
                    }
                }
            });
    },

});
export const { setPosts, setLoading, setError } = postsSlice.actions;
export const selectAllPosts = (state) => state.posts.posts;
export const selectLoading = (state) => state.posts.loading;
export const selectError = (state) => state.posts.error;
export const getPostsByIds = (state, postIds) => {
    return postIds.map((postId) => {
        const postIndex = state.posts.posts.findIndex((post) => post.postId === postId);

        if (postIndex !== -1) {
            return state.posts.posts[postIndex];
        }

        return null;
    });
};
export const getPostByAuthor = (author) => createSelector(
    state => state.posts.posts,
    posts => posts.filter(post => post.author === author)
)
export const { filteredPosts } = postsSlice.actions;
export default postsSlice.reducer;


