import { adminSendMessage, dismissRequest, fetchAllRequests, makeRequest, setIsNewAdminMessageFalse, setIsNewUserMessageFalse, userSendMessage } from '../actions/requests-actions';

import { createSlice } from '@reduxjs/toolkit';

const requestsSlice = createSlice({
    name: 'requests',
    initialState: {
        requests: [],
        error: null,

    },
    reducers: {
        setRequests: (state, action) => {
            return action.payload;
        },

    },
    extraReducers: (builder) => {
        builder
            .addCase(fetchAllRequests.fulfilled, (state, action) => {
                state.requests = action.payload;
            })
            .addCase(makeRequest.fulfilled, (state, action) => {
                state.requests.push(action.payload.request);
            })
            .addCase(dismissRequest.fulfilled, (state, action) => {
                state.requests = state.requests.filter((request) => request.displayName !== action.payload.displayName);
            })
            .addCase(adminSendMessage.fulfilled, (state, action) => {
                const { message, displayName } = action.payload;
                const requestToUpdate = state.requests.find(req => req.displayName === displayName);
                if (requestToUpdate) {
                    if (!requestToUpdate.adminMessages) {
                        requestToUpdate.adminMessages = [];
                    }
                    requestToUpdate.adminMessages.push(message);

                }
            })
            .addCase(userSendMessage.fulfilled, (state, action) => {
                const { message, displayName } = action.payload;
                const requestToUpdate = state.requests.find(req => req.displayName === displayName);
                if (requestToUpdate) {
                    if (!requestToUpdate.userMessages) {
                        requestToUpdate.userMessages = [];
                    }
                    requestToUpdate.userMessages.push(message);

                }
            })
            .addCase(setIsNewUserMessageFalse.fulfilled, (state, action) => {
                const requestToUpdate = state.requests.find(req => req.displayName === action.payload);
                if (requestToUpdate) {
                    requestToUpdate.isNewUserMessage = false;
                }
            })
            .addCase(setIsNewAdminMessageFalse.fulfilled, (state, action) => {
                const requestToUpdate = state.requests.find(req => req.displayName === action.payload);
                if (requestToUpdate) {
                    requestToUpdate.isNewAdminMessage = false;
                }
            })

    },
});

export const { setRequests } = requestsSlice.actions;
export default requestsSlice.reducer;
