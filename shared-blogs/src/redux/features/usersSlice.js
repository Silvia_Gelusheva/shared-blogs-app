import { deleteUserAsyncThunk, fetchAllUsers, makeAdmin, updateUserAsyncThunk } from '../../redux/actions/user-actions';

import { createSlice } from '@reduxjs/toolkit';

const usersSlice = createSlice({
  name: 'users',
  initialState: {
    users: [],
    usersContainer: [],
    status: 'idle',
    error: null,
  },
  reducers: {
		filteredUsers: (state, action) => {
			state.users = state.usersContainer.filter((user) =>
				user.displayName.toLowerCase().includes(action.payload)
			);
		},
	},
  extraReducers: (builder) => {
    builder
      .addCase(fetchAllUsers.pending, (state) => {
        state.status = 'loading';
      })
      .addCase(fetchAllUsers.fulfilled, (state, action) => {
        state.status = 'succeeded';
        state.users = action.payload;
        state.usersContainer = action.payload;
      })
      .addCase(fetchAllUsers.rejected, (state, action) => {
        state.status = 'failed';
        state.error = action.error.message;
      })
      .addCase(deleteUserAsyncThunk.fulfilled, (state, action) => {
        state.users = state.users.filter((user) => user.displayName !== action.payload)
        state.usersContainer = state.usersContainer.filter((user) => user.displayName.displayName !== action.payload)
    })  
      .addCase(updateUserAsyncThunk.fulfilled, (state, action) => {
        const { displayName, updatedData } = action.payload;
        const userIndex = state.users.findIndex(user => user.displayName === displayName);

        if (userIndex !== -1) {
          state[userIndex] = { ...state[userIndex], ...updatedData };
        }
      })
      .addCase(makeAdmin.fulfilled, (state, action) => {
    
        const userIndex = state.users.findIndex(user => user.displayName === action.payload);

        if (userIndex !== -1) { 
          state.users[userIndex].role = 2;
        }
      });
  },
});
export const selectAllUsers = (state) => state.users.users;
export const getUserById = (state, id) => {state.users.users.find(user => user?.id === id);}
export const getUserByDisplayName = (state, displayName) => state?.users?.users?.find(user => user?.displayName === displayName)

export default usersSlice.reducer;

export const { filteredUsers} = usersSlice.actions;
