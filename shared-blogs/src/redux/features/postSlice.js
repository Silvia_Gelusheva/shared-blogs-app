import { DisLikePost, LikePost, fetchPost } from "../actions/post-actions";

import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  post: null,
  likes: [],
  loading: false,
  error: null,
};

const postSlice = createSlice({
  name: "post",
  initialState,
  reducers: {
    setPost: (state, action) => {
      state.post = action.payload;
    },
    setLoading: (state, action) => {
      state.loading = action.payload;
    },
    setError: (state, action) => {
      state.error = action.payload;
    },
    deletePost: (state) => {
      state.post = null;
      state.likes = [];
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(LikePost.fulfilled, (state, action) => {
        state.likes.push(action.payload[1]);
      })
      .addCase(DisLikePost.fulfilled, (state, action) => {
        state.likes = state.likes.filter((displayName) => displayName !== action.payload[1])
      }) 
    .addCase(fetchPost.fulfilled, (state, action) => {
      state.post = action.payload 
  })
  },
});

export const { setPost, setLoading, setError, signOutpost, deletePost } = postSlice.actions;
export const selectPost = (state) => state.post.post;
export const selectLoading = (state) => state.post.loading;
export const selectError = (state) => state.post.error;

export default postSlice.reducer;