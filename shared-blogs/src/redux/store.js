import { combineReducers, configureStore } from '@reduxjs/toolkit';
import { persistReducer, persistStore } from 'redux-persist';

import postSlice from './features/postSlice';
import postsReducer from './features/postsSlice';
import profileReducer from './features/profileSlice';
import requestsReducer from './features/requestsSlice';
import savedReducer from './features/savedSlice';
import storage from 'redux-persist/lib/storage';
import userReducer from './features/userSlice';
import usersReducer from './features/usersSlice';

const persistConfig = {
    key: 'root',
    storage,
};
const rootReducer = combineReducers({

    post: postSlice,
    posts: postsReducer,
    profile: profileReducer,
    saved: savedReducer,
    requests: requestsReducer,
    user: userReducer,
    users: usersReducer

});
 const persistedReducer = persistReducer(persistConfig, rootReducer);

// const store = configureStore({
//     reducer: persistedReducer,

//     middleware: (getDefaultMiddleware) =>
//         getDefaultMiddleware({
//             serializableCheck: false
//         }),
// });
const store = configureStore({
    reducer: rootReducer
});
export const persistor = persistStore(store);
export default store;
