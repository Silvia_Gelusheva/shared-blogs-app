import { auth, db } from '../../firebase/firebase.config';
import { createUserWithEmailAndPassword, signInWithEmailAndPassword, updateProfile } from 'firebase/auth';
import { get, ref, set, update } from 'firebase/database';
import { setError, setLoading, setUser } from '../features/userSlice';

import { createAsyncThunk } from '@reduxjs/toolkit';
import { userProfileActions } from '../features/profileSlice';

export const registerUser = createAsyncThunk(
    'user/register',
    async ({ email, password, displayName, name, lastName, url, role = 1, joinedOn }) => {
        try {
            const userCredential = await createUserWithEmailAndPassword(auth, email, password);

            const { uid } = userCredential.user;
            const userData = {
                displayName,
                email,
                name,
                lastName,
                id: uid,
                avatar: url,
                role,
                joinedOn
            };

       
            await set(ref(db, `users/${displayName}`), userData);

            const user = auth.currentUser;
            await updateProfile(user, { displayName });

     
            return userData;
        } catch (error) {
         
        console.log(error.message)
        }
    }
);
export const logInUser = createAsyncThunk(
    'user/logInUser',
    async ({ email, password }) => {
      try {
        const userCredential = await signInWithEmailAndPassword(auth, email, password);
        const user = userCredential.user;
        
  
        const userSnapshot = await get(ref(db, `users/${user.displayName}`));
        const userData = userSnapshot.val();
  
    
        return userData;
      } catch (error) {
        console.log(error.message)
      }
    }
  );

export const logOutUser = createAsyncThunk('user/logOutUser', async () => {
    try {
        await auth.signOut();
    } catch (error) {
        console.log(error.message);
    }
}
);

export const fetchUser = createAsyncThunk(
    'user/fetchUser',
    async (displayName) => {
        const snapshot = await get(ref(db, `users/${displayName}`));
        return snapshot.val();
    }
);
export const fetchAllUsers = createAsyncThunk(
    'users/fetchAllUsers',
    async () => {
        const snapshot = await get(ref(db, 'users'));
        return Object.values(snapshot.val());
    }
);

export const getUserByName = async (displayName) => {
    const snapshot = await get(ref(db, `users/${displayName}`))

    return snapshot.val()
}

export const fetchUserProfile = (displayName) => async (dispatch) => {
    try {
        const userProfile = await getUserByName(displayName);
        dispatch(userProfileActions.setUserProfile(userProfile));
    } catch (error) {
        console.log(error.message)
    }
};

export const updateUserAsyncThunk = createAsyncThunk("user/updateUserAsyncThunk", async (userData, { getState }) => {
    try {
        const { user } = getState().profile;
        const updatedUser = { ...user, ...userData };
        const databaseRef = ref(db, `users/${user?.displayName}`);
        await update(databaseRef, updatedUser);

        return updatedUser;
    } catch (error) {
        console.log(error.message)
    }
});

export const deleteUserAsyncThunk = createAsyncThunk('users/deleteUserAsyncThunk', async (displayName) => {
    await update(ref(db), {
        [`users/${displayName}`]: null,
    })

    return displayName;
});

export const makeAdmin = createAsyncThunk('users/makeAdmin', async (displayName, { rejectWithValue }) => {
    try {
        await update(ref(db), {
            [`users/${displayName}/role`]: 2,
        });

        return displayName;
    } catch (error) {
        console.error("Error updating database:", error.message);
        return rejectWithValue("Failed to update database");
    }
});