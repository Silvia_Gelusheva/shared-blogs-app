import { get, ref, update } from 'firebase/database';

import { createAsyncThunk } from '@reduxjs/toolkit';
import { db } from '../../firebase/firebase.config';

// ADD / REMOVE / DISPLAY / HANDLE REQUESTS AND CHAT WITH ADMINS
export const makeRequest = createAsyncThunk(
  'requests/makeRequest',
  async ({ request, displayName }) => {
    await update(ref(db), {
      [`requests/${displayName}`]: request,
    })

    return { request, displayName };
  });

export const dismissRequest = createAsyncThunk('requests/dismissRequest', async (request) => {
  await update(ref(db), {
    [`requests/${request.displayName}`]: null,
  });

  return request;
});

export const fetchAllRequests = createAsyncThunk('requests/fetchAllRequests', async () => {
  const snapshot = await get(ref(db, `requests`));
  if (!snapshot.exists()) return []
  return Object.values(snapshot.val());
});

export const adminSendMessage = createAsyncThunk(
  'requests/adminSendMessage',
  async ({ message, displayName }) => {
    const timestamp = Date.now();
    try {
      const snapshot = await get(ref(db, `requests/${displayName}/adminMessages`));
      const existingMessages = snapshot.val() || [];
      const updatedMessages = [...existingMessages, { timestamp, message }];

      await update(ref(db), {
        [`requests/${displayName}/adminMessages`]: updatedMessages,
         [`requests/${displayName}/isNewAdminMessage`]: true,
      });

      return { message: { timestamp, message }, displayName };
    } catch (error) {
      console.error('Error sending admin message:', error);
    }
  }
);

export const userSendMessage = createAsyncThunk(
  'requests/userSendMessage',
  async ({ message, displayName }) => {
    const timestamp = Date.now();
    try {
      const snapshot = await get(ref(db, `requests/${displayName}/userMessages`));
      const existingMessages = snapshot.val() || [];

      const updatedMessages = [...existingMessages, { timestamp, message, displayName }];

      await update(ref(db), {
        [`requests/${displayName}/userMessages`]: updatedMessages,
        [`requests/${displayName}/isNewUserMessage`]: true,
      });

      return { message: { timestamp, message, displayName }, displayName };
    } catch (error) {
      console.error('Error sending user message:', error);
    }
  }
);

export const setIsNewUserMessageFalse = createAsyncThunk('requests/setIsNewUserMessageFalse', async (displayName) => {
  await update(ref(db), {
    [`requests/${displayName}/isNewUserMessage`]: false,
  });

  return displayName;
});

export const setIsNewAdminMessageFalse = createAsyncThunk('requests/setIsNewAdminMessageFalse', async (displayName) => {
  await update(ref(db), {
    [`requests/${displayName}/isNewAdminMessage`]: false,
  });

  return displayName;
});

// export const checkForNewMessages = async (displayName, lastViewedTimestamp) => {
//   const snapshot = await get(ref(db, `requests/${displayName}/adminMessages`));

//   const messages = snapshot.val() || [];

//   const latestMessage = messages[messages.length - 1];
//   if (latestMessage && latestMessage.timestamp > lastViewedTimestamp) {
//     return true;
//   }
//   return false;

// };
// export const checkForNewMessagesFromUser = async (displayName, lastViewedTimestamp) => {
//   const snapshot = await get(ref(db, `requests/${displayName}/userMessages`));

//   const messages = snapshot.val() || [];

//   const latestMessage = messages[messages.length - 1];
//   if (latestMessage && latestMessage.timestamp > lastViewedTimestamp) {
//     return true;
//   }
//   return false;

// };

// export const updateLastViewedTimestamp = async (displayName) => {
//   const timestamp = Date.now();

//   await update(ref(db), {
//     [`requests/${displayName}/lastViewedTimestamp`]: timestamp,
//   });

// };

// export const updateLastViewedTimestampAdmin = async (displayName) => {
//   const timestamp = Date.now();

//   await update(ref(db), {
//     [`requests/${displayName}/lastViewedTimestampAdmin`]: timestamp,
//   });

// };
