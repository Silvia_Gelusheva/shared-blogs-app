import { get, push, ref, update } from 'firebase/database';

import { createAsyncThunk } from '@reduxjs/toolkit';
import { db } from '../../firebase/firebase.config';
import { v4 as uuidv4 } from 'uuid';

// ADD / REMOVE / DISPLAY / LIKE / REPORT / BLOCK BLOGS 
export const addPostToFirebase = createAsyncThunk('posts/addPostToFirebase', async (post) => {
  const { key } = await push(ref(db, 'posts'), { ...post, isBlocked: false })
  await update(ref(db), {
    [`posts/${key}/postId`]: key,
    [`users/${post.author}/posts/${key}`]: true,
  })

  return { ...post, postId: key };
});

export const fetchAllPosts = createAsyncThunk('posts/fetchAllPosts', async () => {
  const snapshot = await get(ref(db, 'posts'));

  return Object.values(snapshot.val());
});

export const getPostByPostId = async (postId) => {
  const snapshot = await get(ref(db, `posts/${postId}`));

  return snapshot.val();
}
export const fetchPost = createAsyncThunk('post/fetchAllPosts', async (postId) => {
  try {
    const snapshot = await get(ref(db, `posts/${postId}`));

    return snapshot.val();
  } catch (error) {
    console.log(error.message);
  }
});

export const updatePostAsyncThunk = createAsyncThunk("posts/updatePostAsyncThunk", async ([postData, post]) => {
  try {

    const updatedPost = { ...post, ...postData };
    const databaseRef = ref(db, `posts/${post.postId}`);
    await update(databaseRef, updatedPost);

    return updatedPost;
  } catch (error) {
    console.log(error.message);
  }
});

export const deletePostAsyncThunk = createAsyncThunk('posts/deletePostAsyncThunk', async ([postId, displayName]) => {
  await update(ref(db), {
    [`posts/${postId}`]: null,
    [`users/${displayName}/posts/${postId}`]: null,
  })

  return [postId, displayName];
});

export const toggleBlockPost = createAsyncThunk('posts/toggleBlockPost', async (post) => {
  try {
    await update(ref(db), {
      [`posts/${post.postId}/isBlocked`]: !post.isBlocked,

    });

    return post;
  } catch (error) {
    console.error("Error updating database:", error.message);

  }

});

export const LikePost = createAsyncThunk('post/LikePost', async ([postId, displayName], { rejectWithValue }) => {
  try {
    await update(ref(db), {
      [`posts/${postId}/likedBy/${displayName}`]: true,
      [`users/${displayName}/likedPosts/${postId}`]: true,
    });

    return [postId, displayName];
  } catch (error) {
    console.error("Error updating database:", error.message);
    return rejectWithValue("Failed to update database");
  }
});

export const DisLikePost = createAsyncThunk('post/DisLikePost', async ([postId, displayName]) => {
  try {
    await update(ref(db), {
      [`posts/${postId}/likedBy/${displayName}`]: null,
      [`users/${displayName}/likedPosts/${postId}`]: null,
    })
    return [postId, displayName];
  } catch (error) {
    console.log(error.message);
  }
});


export const reportPost = createAsyncThunk('posts/reportPost', async ([post, displayName, selectedArguments, message]) => {

  try {
    let id = uuidv4();
    const report = { reportId: id, postId: post.postId, reportedBy: displayName, selectedArguments: selectedArguments, message: message }

    await update(ref(db), {

      [`allReports/${post.postId}/${id}`]: report,
      [`posts/${post.postId}/reports/${id}`]: report,
    })

    return report;

  } catch (error) {
    console.log("Error adding report: ", error.message);
    throw error;
  }
});

export const dismissReport = createAsyncThunk('posts/dismissReport', async ([postId, reportId, displayName]) => {

  try {
    await update(ref(db), {

      [`allReports/${postId}/${reportId}`]: null,
      [`posts/${postId}/reports/${reportId}`]: null,
    })

    return [postId, displayName];

  } catch (error) {
    console.log("Error dismissing report: ", error.message);
    throw error;
  }
});


export const dismissAllPostReports = createAsyncThunk('posts/dismissAllPostReports', async (postId) => {

  try {
    await update(ref(db), {

      [`allReports/${postId}`]: null,
      [`posts/${postId}/reports`]: null,
    })

    return [postId];

  } catch (error) {
    console.log("Error dismissing report: ", error.message);
    throw error;
  }
});

export const fetchAllReports = createAsyncThunk('posts/fetchAllReports', async () => {
  const snapshot = await get(ref(db, `allReports`));
  if (!snapshot.exists()) return []
  return snapshot.val();
});

export const toggleLikePost = createAsyncThunk(
  'posts/toggleLikePost',
  async ([postId, displayName], { getState, dispatch }) => {
    const state = getState();
    const post = state.posts.posts.find((post) => post.postId === postId);

    if (!post) {
      console.error("Post not found");
      return;
    }

    const isLiked = post.likedBy && post.likedBy[displayName];

    try {
      if (isLiked) {
        await dispatch(DisLikePost([postId, displayName]));
      } else {
        await dispatch(LikePost([postId, displayName]));
      }
    } catch (error) {
      console.error("Error toggling like:", error.message);
    }
    return [postId, displayName]
  }
);