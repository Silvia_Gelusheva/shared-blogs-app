import { get, ref, update } from 'firebase/database';

import { createAsyncThunk } from '@reduxjs/toolkit';
import { db } from '../../firebase/firebase.config';

// ADD / REMOVE / DISPLAY SAVED BLOGS 
export const savePost = createAsyncThunk(
    'saved/savePost',
    async ([postId, displayName]) => {
      await update(ref(db), {
        [`savedPosts/${displayName}/${postId}`]: true,
        [`posts/${postId}/savedBy/${displayName}`]: true,
      })
  
      return postId;
    });
  
  export const removeFromSaved = createAsyncThunk('saved/removeFromSaved', async ([postId, displayName]) => {
    await update(ref(db), {
      [`savedPosts/${displayName}/${postId}`]: null,
      [`posts/${postId}/savedBy/${displayName}`]: null,
    })
    return postId;
  });
  
  
  export const fetchAllSavedPostsByUser = createAsyncThunk('saved/fetchAllSavedPostsByUser', async (displayName) => {
    const snapshot = await get(ref(db, `savedPosts/${displayName}`));
    if (!snapshot.exists()) return []
    return Object.keys(snapshot.val());
  });
  
  export const getSavedPostsByIds = async (postIds) => {
    const snapshot = await get(ref(db, `posts`));
    if (!snapshot.exists()) return []
    const allPosts = snapshot.val();
    const favoritePosts = postIds?.map(postId => allPosts[postId]);
    return favoritePosts;
  
  };
   export const deleteSavedPostByUser = createAsyncThunk('saved/deleteSavedPostByUser', async (displayName) => {
    await update(ref(db), {
      [`savedPosts/${displayName}`]: null,
    })
    return displayName;
  });
  